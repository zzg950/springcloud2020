package com.zzg.springcloud.order.config;


import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {

    /**
     * 创建 RestTemplate 的 Bean对象
     * Eureka 默认依赖了 Ribbon 包，所以使用 Ribbon 的时候不需要额外引入依赖
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
