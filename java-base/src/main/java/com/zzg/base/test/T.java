package com.zzg.base.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class T {

    public static void main(String[] args) throws ParseException {
        String str = "123";
        String str2 = "张志刚";
        Object obj = "张志刚2";
        System.out.println(str.getClass() == str2.getClass());
        System.out.println(str.getClass() == obj.getClass());
        System.out.println("str.getClass() == String.class = " + (str.getClass() == String.class));

        List<String> strList = new ArrayList<>();
        List<Integer> intList = new ArrayList<>();
        List<String> linkedList = new LinkedList<>();
        System.out.println(strList.getClass() == intList.getClass());
        System.out.println("strList.getClass() == linkedList.getClass() = " + (strList.getClass() == linkedList.getClass()));
        System.out.println("intList.getClass() = " + intList.getClass());

        System.out.println("str instanceof String = " + (str instanceof Object));
        System.out.println("strList instanceof List = " + (strList instanceof List));
        System.out.println("strList instanceof ArrayList = " + (strList instanceof ArrayList));
        System.out.println("strList instanceof LinkedList = " + (strList instanceof LinkedList));

    }


}
