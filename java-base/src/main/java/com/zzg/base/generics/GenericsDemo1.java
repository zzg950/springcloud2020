package com.zzg.base.generics;

import com.zzg.base.entity.Teacher;

import java.util.*;

public class GenericsDemo1 {


    public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Teacher teacher = GenericsDemo1.createInstance(Teacher.class);
        teacher.setId(12);
        System.out.println(teacher.getId());
        Teacher teacher2 = (Teacher) Class.forName(Teacher.class.getName()).newInstance();
        teacher2.setName("张志刚");
        System.out.println(teacher2.getName());

        GenericsDemo1.test111("123");
        GenericsDemo1.test111(123);


        List<Object> list = new ArrayList<>();
        Set<Object> set = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
            set.add("set_index_" + i);
        }

        GenericsDemo1.printCollection(list);
        GenericsDemo1.printCollection(set);
    }

    public static void abc(List list) {

    }

    public static void abc1(List<?> list) {

    }

    public <T> void doSomething(T t) {

    }

    /**
     * 通过泛型和反射创建任意的对象
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createInstance(Class<T> clazz) throws IllegalAccessException, InstantiationException {
        return clazz.newInstance();
    }

    /**
     * @param coll
     * @功能：打印列表对象
     */
    public static void printCollection(Collection<Object> coll) {
        if (coll == null || coll.size() == 0) {
            System.out.println("集合为空");
            return;
        }
        for (Object obj : coll) {
            if (obj != null) {
                System.out.println(obj.toString());
            } else {
                System.out.println("当前下标的值为 ~ null");
            }
        }
    }


    public static void test111(Object obj) {
        System.out.println(obj);
    }


}
