package com.zzg.base.generics;

import com.zzg.base.generics.entity.GenericsEntity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义泛型使用的demo
 */
public class GenericsDemo {

    public static void main(String[] args) {
        GenericsEntity gd = new GenericsEntity(123, "张志刚", Arrays.asList("1,2,3,4,5,6,7".split(",")));
        System.out.println(gd);
        System.out.println(new GenericsEntity(123, "张志刚", Arrays.asList("1,2,3,4,5,6,7".split(","))));

        GenericsDemo demo = new GenericsDemo();
        demo.noSpecifyType();
        System.out.println("==========================================");
        demo.specifyType();
    }

    /**
     * 1. 创建对象时不指定类型
     * 2. 获取结果时，需要进行强制转换
     */
    public void noSpecifyType() {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("name", "张志刚");
        mapData.put("age", 11);
        System.out.println("mapData = " + mapData);

        GenericsEntity entity = new GenericsEntity();
        entity.setCode(0);
        entity.setMessage("SUCCESS");
        entity.setData(mapData);

        // 需要强制类型转换
        Map<String, Object> mapResult = (Map<String, Object>) entity.getData();
        System.out.println("mapResult = " + mapResult);
    }

    /**
     * 1. 创建对象时指定类型
     * 2. 获取结果时，不需要进行强制转换
     */
    public void specifyType() {
        GenericsEntity<String> entity = new GenericsEntity();
        entity.setData("不需要强制转换");

        String res = entity.getData();
        System.out.println("res = " + res);
    }
}
