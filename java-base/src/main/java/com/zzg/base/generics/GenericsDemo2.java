package com.zzg.base.generics;

import com.zzg.base.entity.Animal;
import com.zzg.base.entity.Teacher;

import java.math.BigDecimal;
import java.util.*;


/**
 * 泛型中的继承
 * 1. 泛型中的继承关系跟JAVA中的继承关系不同
 * 2. 比如定义一个方法，参数是Number对象，那么在调用方法时如果传入的是Integer或者Float是可以运行的
 * 3. 如果参数是List<Number>，那么在调用参数是传入的是List<Integer>等则调不通，说明泛型内部的对象继承关系不被承认
 * 4. 如果需要使用泛型的继承，则需要使用 ? extend T（上边界） 或者 ? super T （下边界）处理
 */

public class GenericsDemo2 {

    public static void main(String[] args) {
        // Java 的继承 - 方法定义的 Number, 实际参数是 Integer 或者 Float都可以
        GenericsDemo2.method1(15, 16);
        GenericsDemo2.method1(100, 16.16);
        GenericsDemo2.method1(15.15, 16.16);


        List<Integer> list1 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list1.add(i + 1);
        }
        // Java 泛型的继承 - 方法定义的是List<Number>, 参数是List<Integer>是不行的
//        GenericsDemo2.method2(list1);

        // Java 泛型的继承 - 如果定义方法是使用泛型继承List<? extends Number>
        // ? extends Number 表示泛型上界，表示只要泛型内部的参数是 Number 的子类即可
        // 主要用于读取数据
        GenericsDemo2.method3(list1);

    }

    public static void method1(Number n1, Number n2) {
        System.out.println(String.format("n1 = %s, n2 = %s", n1, n2));
    }

    public static void method2(List<Number> list) {
        for (Number number : list) {
            System.out.println("method2 --> " + number);
        }
    }

    public static void method3(List<? extends Number> list) {
        for (Number number : list) {
            System.out.println("method3 --> " + number);
        }
    }

}
