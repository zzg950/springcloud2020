package com.zzg.base.number;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * java 中各个整数类型可以取值的范围
 * 1. byte的取值范围为-128~127，占用1个字节（-2的7次方到2的7次方-1）
 * 2. short的取值范围为-32768~32767，占用2个字节（-2的15次方到2的15次方-1）
 * 3. Integer的取值范围为 -2147483648 ~ 2147483647: -2的31次方 --> 2的31次方-1
 * 4. long的取值范围为（-9223372036854774808~9223372036854774807），占用8个字节（-2的63次方到2的63次方-1）
 * <p>
 * 所以，不管int和long的数字范围再大也是有限度的，所以无法计算更大的数值运算
 * 而 BigInteger类的取值范围要比 Integer 和 Long 要大得多，可以支持任意精度的整数。
 */
public class BigIntegerDemo {

    public static void main(String[] args) {
        // 基本运算
        BigInteger integer1 = new BigInteger("9876543211234567890987654321");
        BigInteger integer2 = new BigInteger("9876543211234567890987654321789");

        System.out.println("integer1.add(integer2) = " + integer1.add(integer2));
        System.out.println("integer1.subtract(integer2) = " + integer1.subtract(integer2));
        System.out.println("integer1.multiply(integer2) = " + integer1.multiply(integer2));
        System.out.println("integer1.divide(integer2) = " + integer1.divide(integer2));
        System.out.println("integer1.divide(integer2) = " + integer2.divide(integer1));

        // 平方 - 3次方
        BigInteger integer3 = new BigInteger("4");
        System.out.println("integer3.pow(3) = " + integer3.pow(3));

        // 取模
        System.out.println(integer1.mod(new BigInteger("15")));

        // 位移
        System.out.println("integer1.shiftLeft(5) = " + integer1.shiftLeft(5));
        System.out.println("integer2.shiftRight(5) = " + integer2.shiftRight(5));

    }

}
