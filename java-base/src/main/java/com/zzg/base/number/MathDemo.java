package com.zzg.base.number;


/**
 * java 中用于对数字进行计算和处理的对象
 * 里面提供了很多静态方法可以直接使用，用于一些常用的数字的问题
 */
public class MathDemo {

    public static void main(String[] args) {

        // 最大值和最小值 max || min
        System.out.println("Math.max(100, 100.01) = " + Math.max(100, 100.01));
        System.out.println("Math.max(100, 200) = " + Math.max(100, 200));

        // 运算
        System.out.println("Math.multiplyExact(100, 100) = " + Math.multiplyExact(100, 100));

        /**
         * 三个与取整有关的方法 : ceil + floor + round
         * 1. ceil : 英文含义是天花板的意思，该方法表示向上（取大值）取整; 11.3 --> 12; -11.3 --> -11
         * 2. floor : 英文含义是地板的意思，该方法表示向下（取小值）取整;  11.3 --> 11; -11.3 --> -12
         * 3. round : 表示四舍五入，Math.round 的算法是 Math.floor(x+0.5)，表示将原来的数字加上0.5之后取地板地板值
         *            11.5 --> 12; -11.5 --> -11.5 + 0.5 = -11 --> -11; -11.6 + 0.5 = -11.1 --> -12
         */

    }


}
