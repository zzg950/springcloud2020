package com.zzg.base.number;


import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * https://www.cnblogs.com/shy1766IT/p/10262325.html
 * BigDecimal 数据格式化输出
 * DecimalFormat，是NumberFormat的具体实现子类
 */
public class DecimalFormatDemo {


    public static void main(String[] args) {

        /**
         * DecimalFormat，是NumberFormat的具体实现子类
         * 构造方法可以直接定义模式，也可以由applyPattern设置模式
         * 如果定义的
         * 1. #0.00   保留两位小数，对应位上无数字填充0，使用的是 ROUND_HALF_UP 模式，切记double精度造成的问题
         * 2. #000.00 保留两位小数，对应位上无数字填充0，
         * 3. #0.00AA 保留两位小数，在最终的结果后面补AA  1.235 --> 1.24AA
         *
         *
         */
        DecimalFormat df = new DecimalFormat("#0.00AA");
        System.out.println(df.format(new BigDecimal("121.235")));     // 1.24
        System.out.println(df.format(new BigDecimal("11221.2349")));    // 1.23
        System.out.println(df.format(new BigDecimal("1.2")));       // 1.20
        System.out.println(df.format(1.23 + 0.005));            // 1.23

        System.out.println("====================");
        // #：位置上无数字不显示
        df.applyPattern("#.##");
        System.out.println(df.format(34523.00));
        System.out.println(df.format(34523.556));

        // 0:位置上无数字显示0
        df.applyPattern("0.00");
        System.out.println(df.format(34523.00));
        System.out.println(df.format(34523.556));

        // 加负数显示
        df.applyPattern("0.00");
        System.out.println(df.format(34523.00));
        System.out.println(df.format(-34523.556));

        // 逗号分隔
        df.applyPattern("0,000.00");
        System.out.println(df.format(345227543.00));
        System.out.println(df.format(-345227543.556));

        // 百分位 - 会自动把数字扩大100倍再追加 % 符号
        df.applyPattern("0.00%");
        System.out.println(df.format(345227543.00));
        System.out.println(df.format(-345227543.556));

        // 千分位 - 会自动把数字扩大1000倍再追加 ‰ 符号
        df.applyPattern("0.00\u2030");
        System.out.println(df.format(0.123456));
        System.out.println(df.format(0.345678));

        // 格式后面加单位符号
        df.applyPattern("0.00 KG");
        System.out.println(df.format(14.56));


        // String.format
        // 保留两位小数，个位数及小数点后两位无数字填充0，四舍五入
        System.out.println(String.format("%.2f", 0.2));// 0.20
        System.out.println(String.format("%.2f", 0.235));// 0.24
        System.out.println(String.format("%.2f", 0.236));// 0.24
        System.out.println(String.format("%.2f", 42.0));// 42.00
    }


}
