package com.zzg.base.collection.map;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

/**
 * 有序，按自然顺序的map
 * 可以自己指定排序规则
 */
public class TreeMapDemo {

    public static void main(String[] args) {
        Map<String, Object> map = new TreeMap<>();

        Map<String, Object> map2 = new TreeMap<>(new KeyLengthComparator());
    }

    private static class KeyLengthComparator implements Comparator<String> {
        @Override
        public int compare(String s1, String s2) {
            return Integer.compare(s1.length(), s2.length());
        }
    }

}
