package com.zzg.base.collection.list;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 有序（按添加顺序）、可重复集合、可添加 null
 */

/**
 * 特点
 * 1. 双向链表结构：LinkedList 中的每个元素（节点）都包含指向前一个节点和后一个节点的指针，可以实现双向遍历。
 * 2. 动态大小：LinkedList 的大小可以根据需要动态地增加或减小。
 * 3. 插入和删除快速：在 LinkedList 中插入和删除元素时，只需要修改前后节点的指针，不需要移动其他元素，因此插入和删除速度比较快。
 * 4. 实现了 List 和 Deque 接口：可以直接使用 List 和 Deque 接口的方法，如添加、删除、获取元素等。
 */

/**
 * 优点
 * 1. 插入和删除元素效率高：由于使用链表结构，插入和删除元素时只需要修改相邻节点的指针，相对于 ArrayList 而言效率更高。
 * 2. 无需预先分配内存空间：LinkedList 的内部结构不需要连续的内存空间，可以根据需要动态分配节点，节省内存空间。
 */

/**
 * 缺点
 * 1. 随机访问性能差：由于链表结构的特性，随机访问元素时需要从头节点开始遍历，直到找到目标节点，因此随机访问的性能较差，时间复杂度为 O(n)。
 * 2. 占用额外空间：LinkedList 需要为每个节点额外存储指针信息，相对于 ArrayList 来说会占用更多的内存空间。
 * 3. 不适合大量数据存储：由于每个节点都需要额外存储指针信息，当存储大量数据时，可能会占用较大的内存空间。
 */
public class LinkedListDemo {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        list.add("AA");
        list.add("BB");
        list.add("AA");
        list.add("CC");
        list.add(null);
        list.add(5, "FF");
        list.add("GG");
        System.out.println(list.size());
        System.out.println(list);

        System.out.println("list instanceof List = " + (list instanceof List));
        System.out.println("list instanceof LinkedList = " + (list instanceof LinkedList));
    }
}
