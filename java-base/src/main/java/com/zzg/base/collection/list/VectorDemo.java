package com.zzg.base.collection.list;


import java.util.Vector;

/**
 * 所有操作同步进行的 list 集合
 */
public class VectorDemo {

    public static void main(String[] args) {
        Vector<String> list = new Vector<>(100);
        list.add("AAA");
        list.add("BBB");
        list.add("AAA");
        list.add("CCC");
        list.add(null);
        list.add(5, "FFF");
        list.add("GGG");
        System.out.println(list.size());
        System.out.println(list);
    }

}
