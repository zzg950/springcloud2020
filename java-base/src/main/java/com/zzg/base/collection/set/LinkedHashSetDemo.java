package com.zzg.base.collection.set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 有序（按添加顺序排序），不可重复
 */
public class LinkedHashSetDemo {

    public static void main(String[] args) {
        Set<String> set = new LinkedHashSet<>();
        for (int i = 0; i < 10; i++) {
            set.add("T" + i);
        }
        set.add("T5");
        System.out.println(set);
        set.forEach(System.out::println);
    }
}
