package com.zzg.base.collection.list;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 有序（按添加顺序）、可重复集合、可添加 null
 * 其实就是动态数组，如果可提前知道list的预估长度，那么在创建时可以直接指定大小，避免重复扩容造成资源浪费
 * 在添加元素时，先判断容量是否足够，不够则扩容，扩容按1.5倍进行扩容。扩容会新建数组，复制数组。
 * 可指定下标进行赋值，但是不能越界
 * 获取元素时，先查看下标是否越界，然后再获取对应的值
 */

/**
 * 特点
 * 1. 动态大小：ArrayList 的大小可以根据需要动态地增加或减小。
 * 2. 索引访问：可以使用索引来访问元素，因为 ArrayList 实现了 List 接口。
 * 3. 可以存储任意类型：ArrayList 可以存储任意类型的对象，包括基本数据类型（通过自动装箱）和对象类型。
 * 4. 有序存储：ArrayList 中的元素按添加的顺序进行存储，可以保持元素的顺序。
 * 5. 可重复添加： 存储对象可以重复存储
 */

/**
 * 优点
 * 1. 随机访问快速：由于 ArrayList 内部通过数组实现，可以通过索引快速定位和访问元素。
 * 2. 动态增长：ArrayList 的大小可以根据需要动态增长，避免了固定大小数组的限制。
 * 3. 方便的插入和删除：向 ArrayList 中插入和删除元素时，自动调整底层数组的大小，而不需要手动处理。
 */

/**
 * 缺点
 * 1. 频繁的插入和删除操作：在 ArrayList 中频繁执行插入和删除操作时，需要移动后续元素，导致性能下降。
 * 2. 不适合存储大量数据：ArrayList 内部使用数组实现，当存储大量数据时，可能占用很大的内存空间。
 * 3. 非线程安全：ArrayList 不是线程安全的，如果多个线程同时修改 ArrayList，可能会导致数据不一致的问题。
 */

/**
 * 需要注意的是，如果对于频繁的插入和删除操作，可以考虑使用 LinkedList，它的插入和删除操作性能更好。
 * 而如果需要线程安全的集合，可以使用 Vector 或者 Collections 工具类中提供的线程安全的集合方法来操作 ArrayList。
 */
public class ArrayListDemo {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(100);
        list.add("A");
        list.add("B");
        list.add("A");
        list.add("C");
        list.add(null);
        list.add(5, "F");
        list.add("G");
        System.out.println(list.size());
        System.out.println(list);

        int len = list.size();
        System.out.println(len >> 1 << 1);
        System.out.println(len << 1 << 1);

        System.out.println("list instanceof List = " + (list instanceof List));
        System.out.println("list instanceof ArrayList = " + (list instanceof ArrayList));
    }

}
