package com.zzg.base.collection.set;


import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * 有序（按自然顺序排序），不可重复集合
 * 可以自己指定排序规则
 */
public class TreeSetDemo {

    public static void main(String[] args) {
        Set<String> set = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(o1.length(), o2.length());
            }
        });
        set.add("11"); set.add("CDD"); set.add("AA"); set.add("TEE"); set.add("AA");

        System.out.println(set);
        set.forEach(System.out::println);
    }

}
