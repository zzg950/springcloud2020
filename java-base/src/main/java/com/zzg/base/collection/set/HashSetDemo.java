package com.zzg.base.collection.set;


import java.util.HashSet;
import java.util.Set;

/**
 * 乱序、不可重复集合
 */
public class HashSetDemo {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            set.add("T" + i);
        }
        System.out.println(set);
        set.forEach(System.out::println);

    }

}
