package com.zzg.base.enums;

/**
 * 简单的枚举类型 - 只有值定义
 * 当定义一组常量时，建议定义一组枚举类
 * 比如: 定义程序支持的支付方式
 */
public enum PaymentTypeEnum {

    /**
     * 支付宝支付
     */
    aliPay,

    /**
     * 微信支付
     */
    weixinPay,

    /**
     * 京东支付
     */
    jdPay,

    hwPay,

    bankPay;


    public static void main(String[] args) {
        System.out.println(PaymentTypeEnum.aliPay.getClass());
        System.out.println(PaymentTypeEnum.aliPay.name());
        System.out.println(PaymentTypeEnum.aliPay.toString());
    }

}
