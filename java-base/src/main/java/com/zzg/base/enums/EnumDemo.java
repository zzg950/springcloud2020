package com.zzg.base.enums;

public class EnumDemo {


    public static void main(String[] args) {

        System.out.println("WeekdayEnum.MONDAY = " + WeekdayEnum.MONDAY);
        System.out.println("WeekdayEnum.MONDAY.chineseName = " + WeekdayEnum.MONDAY.getChineseName());
        System.out.println("WeekdayEnum.MONDAY.getCode() = " + WeekdayEnum.MONDAY.getCode());
        System.out.println("WeekdayEnum.MONDAY.getShortName() = " + WeekdayEnum.MONDAY.getShortName());

        System.out.println("WeekdayEnum.MONDAY.name() = " + WeekdayEnum.MONDAY.name());
        System.out.println("WeekdayEnum.MONDAY.toString() = " + WeekdayEnum.MONDAY.toString());
    }

}
