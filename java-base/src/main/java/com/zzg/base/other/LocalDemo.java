package com.zzg.base.other;

import java.util.Locale;

public class LocalDemo {

    public static void main(String[] args) {
        Locale locale = Locale.getDefault();
        System.out.println("locale.getCountry() = " + locale.getCountry());
        System.out.println("locale.getLanguage() = " + locale.getLanguage());
        System.out.println("locale.getDisplayCountry() = " + locale.getDisplayCountry());
        System.out.println("locale.getScript() = " + locale.getScript());
    }
}
