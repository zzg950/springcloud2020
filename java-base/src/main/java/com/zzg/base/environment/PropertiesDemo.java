package com.zzg.base.environment;

import java.io.*;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Java 开发中，需要将一些易变的配置参数放置再 XML 配置文件或者 properties 配置文件中。
 * XML 配置文件需要通过 DOM 或 SAX 方式解析，而读取 properties 配置文件就比较容易。
 * properties 使用的是 ISO-8859-1的编码，即便在idea中设置了UTF-8也不一定有用，需要单独处理加载后的数据流或者读取出来的结果
 * 无论是 Properties 还是 ResourceBundle 都不能直接解析Map、Array、List
 *
 * 乱码问题 : 关于乱码问题：请确保Properties文件（包括ResourceBundle）编码格式为ISO-8859-1。Java的标准假定Properties文件编码格式为ISO-8859-1。用其它格式会比较麻烦。 IDEA上的PropertiesFiles默认可能不是ISO-8859-1，因此很可能造成乱码问题。在IDEA->File->Settings->Editor->FileEncodings->Properties Files 设为ISO-8859-1，勾选 Transparent native-to-ascii conversion。在更换默认编码的之前，备份Properties文件
 *
 * 下面提供了三种方法读取properties配置文件
 *      第一种和第二种类似，都是将配置文件load到Properties中，
 *      第三种resourceBundleTest最方便，直接使用ResourceBundle对象来获取结果
 *
 * https://blog.csdn.net/qq_42831771/article/details/122062272
 *
 * Properties properties = new Properties();
 *      public synchronized void load(Reader reader);
 *      public synchronized void load(InputStream inStream);
 *
 *
 * java 如何运行 jar 包指定类的 main 方法 - 注意要指定类的全限定名, 即包含包名
 * java -cp java-base-1.0-SNAPSHOT.jar com.zzg.base.environment.PropertiesDemo
 * 1. java 是指使用 java命令去运行程序
 * 2. -cp 指的是 classpath 的缩写
 * 3. java-base-1.0-SNAPSHOT.jar 需要运行的jar包文件
 * 4. com.zzg.base.environment.PropertiesDemo 运行这个类的 main 函数
 */
public class PropertiesDemo {

    public static void main(String[] args) {
//        resourceTest();
//        ClassLoaderTest();
//        InputStreamTest();
        resourceBundleTest();
    }

    /**
     * 1.ClassLoader读取配置文件
     */
    public static void ClassLoaderTest() {
        Properties properties = new Properties();
        try {
            // 如果配置文件在 classpath 目录下 - 具体查看当前类的 resourceTest() 方法
            InputStream resourceAsStream = PropertiesDemo.class.getClassLoader().getResourceAsStream("config-dev.properties");
//            properties.load(resourceAsStream);  // 有可能导致乱码 - properties文件默认的编码方式
            properties.load(new InputStreamReader(resourceAsStream, "UTF-8"));
            resourceAsStream.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String applicationName = properties.getProperty("spring.application.name");
        System.out.println("applicationName = " + applicationName);
        String userDir = properties.getProperty("user.dir");
        System.out.println("userDir = " + userDir);
        String username = properties.getProperty("username");
        System.out.println("username = " + username);
    }

    /**
     * 通过 java.util.ResourceBundle 类来读取，这种方式比使用 Properties 要方便一些
     * 如果引入有Spring依赖，则可以使用Spring的工具类PropertiesLoaderUtils处理
     * 通过 ResourceBundle.getBundle() 静态方法来获取（ResourceBundle是一个抽象类）
     * 这种方式来获取properties属性文件不需要加.properties后缀名，只需要文件名
     * 读取的开始位置也是 classpath 目录下
     * 解决中文乱码问题比较麻烦
     *
     * 1. 可解析普通字符串
     * 2. 可解析数组
     */
    public static void resourceBundleTest() {
        ResourceBundle resource = ResourceBundle.getBundle("config-dev");
        String applicationName = resource.getString("spring.application.name");
        System.out.println("applicationName = " + applicationName);
        String userDir = resource.getString("user.dir");
        System.out.println("userDir = " + userDir);
        String username = resource.getString("username");
        System.out.println("username = " + username);

        try {
            // 处理中文乱码问题
            String username2 = new String(resource.getString("username").getBytes("ISO-8859-1"), "UTF-8");
            System.out.println("username2 = " + username2);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * InputStream 读取配置文件
     */
    public static void InputStreamTest() {
        Properties properties = new Properties();
        // 使用InPutStream流读取properties文件
        try {
            // 需要使用具体路径才可以找到相应文件
            String filePath = PropertiesDemo.class.getResource("/").getPath() + "config-dev.properties";
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
            properties.load(bufferedReader);
            bufferedReader.close();

            // 或者直接使用
            String basePath = System.getProperty("user.dir");
            InputStream envIn = new FileInputStream(basePath + "/conf/env.properties");  // 可将文件放在指定位置
            properties.load(envIn);
            envIn.close();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String applicationName = properties.getProperty("spring.application.name");
        System.out.println("applicationName = " + applicationName);
    }


    /**
     * 1. 获取classpath路径
     */
    public static void resourceTest() {
        // 获取当前类的根目录 - 也就是classpath下的目录 - 如果是在jar包内部，则此方法返回空
        URL resource0 = PropertiesDemo.class.getResource("/");
        System.out.println(resource0);

        // 获取当前类所在的目录 - 也就是classpath下的具体位置 - 如果是在jar包里面的话
        URL resource1 = PropertiesDemo.class.getResource("");
        System.out.println(resource1);

        URL resource2 = PropertiesDemo.class.getClassLoader().getResource("");
        System.out.println(resource2);

        URL resource3 = PropertiesDemo.class.getClassLoader().getResource("/");
        System.out.println(resource3);
    }
}
