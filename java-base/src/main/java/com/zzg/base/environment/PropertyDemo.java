package com.zzg.base.environment;


import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

/**
 * 1. 属性是JAVA预制的，其他程序不可用
 * 2. System.getProperties() 获取所有的系统属性
 * 3. System.getProperty(key) 根据key获取具体的属性
 * 4. System.setProperty(key, value) 设置属性，用于在其他地方获取使用，可在运行时进行改变，改变之后整个程序后续获取的都将产生变化
 * 5. 通过 -D 设置启动参数，例如 : -DauthorName=zzg -Dspring.profiles.active=dev
 */
public class PropertyDemo {

    private static final String format = "property : key = %-30s  value = %s";

    public static void main(String[] args) {
        System.out.println("==========System.getProperties()==========");
        // System.getProperties() 获取所有系统属性
        Properties properties = System.getProperties();
        // 使用 forEach 循环所有的 key - value
        properties.forEach((key, value) -> {
//            System.out.println(String.format(format, key, value));
        });
        // 使用枚举循环所有 key - value
        Enumeration<?> enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            Object key = enumeration.nextElement();
            Object value = System.getProperty(key.toString());
//            System.out.println(String.format(format, key, value));
        }
        // 强转 map 循环所有 key - value; 因为 Properties 是 HashTable 的子类
        Map<Object, Object> map = System.getProperties();
        for (Object key: map.keySet()) {
//            System.out.println(String.format(format, key, map.get(key)));
        }

        System.out.println("==========System.getProperty(key)==========");
        // 根据 key 获取具体的变量值  user.dir、 java.version
        String key = "user.dir";
        String value = System.getProperty(key);
        System.out.println(String.format(format, key, value));

        System.out.println("==========System.setProperty(key,value)==========");
        String chromePath = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
        System.setProperty("webdriver.chrome.driver", chromePath);   // selenium 设置 chrome 启动参数
        System.out.println(String.format(format, "webdriver.chrome.driver", System.getProperty("webdriver.chrome.driver")));


        System.out.println("==========-D在启动参数设置属性==========");
        //java -jar xxx.jar -DauthorName=zzg -Dsrping.profiles.active=dev
        // 或者在idea的启动参数里面设置 -DauthorName=zzg -Dsrping.profiles.active=dev
        key = "authorName";
        System.out.println(String.format(format, key, System.getProperty(key)));
        key = "srping.profiles.active";
        System.out.println(String.format(format, key, System.getProperty(key)));

    }

}
