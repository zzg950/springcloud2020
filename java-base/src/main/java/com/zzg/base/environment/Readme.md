
## 概述
System.getenv(); 获取的是操作系统层面的环境变量，比如配置的JAVA_HOME，PATH等。

System.getProperty(); 获取的是java启动脚本设置的参数 如 java -jar -Dsrping.profiles.active=dev。
此时使用 System.getProperty("srping.profiles.active") 返回的是 dev.

在书写的时候建议有个优先级概念，比如先使用System.getProperty()获取，如果没有再从System.getenv()获取，如果还没有则进行相应的业务处理。

## 环境变量
Java中的环境变量分为多种类型
1. 操作系统层面设置的环境变量 - 环境变量
2. 启动时配置的环境变量 - 属性
3. 配置文件配置的环境变量 - 配置文件

属性与环境变量（系统变量）的区别

比较项 | 属性 | 环境变量 | 配置文件
---|---|---|---
运行时是否可修改 | 可以 | 不可以 | 修改后需重新读取解析文件
有效范围 | 仅在java平台中有效 | 系统里所有程序都有效 | 仅在java平台有效
创建的时机 | 打包应用时属性必须存在 | 可以在任何时候创建环境变量 | 在程序读取文件后创建


### 操作系统层面设置的环境变量
众所周知，window或者linux操作系统安装完JDK之后都会对系统进行环境变量的设置，系统设置的环境变量是可以通过 System.getenv(); 来取得的。

### 启动时配置的环境变量
JAVA启动脚本可以设置启动参数，有一些是JVM固定格式的，比如 -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:MaxDirectMemorySize=5M

那么通过-D作为前缀可以设置自定义的参数，比如 -Dsrping.profiles.active=dev。

那么这种方式设置的环境变量可以通过 System.getProperty(); System.getProperty("srping.profiles.active"); 来获取

### 配置文件配置的环境变量
Java 开发中，需要将一些易变的配置参数放置在XML配置文件或者properties配置文件中。XML配置文件需要通过DOM和SAX方式解析，而读取配置文件JDK则有专门的API方案。

## 参考

Windows 10 环境变量 (用户变量与系统变量)
https://blog.csdn.net/chengyq116/article/details/105900122/





java 如何运行 jar 包指定类的 main 方法
https://blog.csdn.net/u012745499/article/details/123512262

Javac -cp 和 Java -cp 命令解释
https://blog.csdn.net/lvjingWn/article/details/73605638

Java打包成jar并执⾏jar包中指定的main⽅法
https://wenku.baidu.com/view/d4e8ccef0ba1284ac850ad02de80d4d8d15a0136.html