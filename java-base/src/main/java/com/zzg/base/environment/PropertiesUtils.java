package com.zzg.base.environment;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 1. 环境变量的配置文件内容读取的工具类
 * 2. 通过老方法 Properties 类来获取属性
 */
public class PropertiesUtils {

    private static Properties prop = new Properties();
    public static String basePath = System.getProperty("user.dir");

    // -Dspring.profiles.active=dev 设置的环境变量，通过环境变量来决定读取哪个文件，默认是 dev
    public static String active = System.getProperty("spring.profiles.active", "dev");

    // 配置文件中的属性 - 设置成静态变量 - 在初始化时进行赋值 - 后面其他地方可直接使用 PropertiesUtils.rootDir 获取

    // String 类型直接获取
    public static String rootDir = "";
    public static String userDir = "";
    public static String username = "";

    // Array 无法自动解析 - 需要utils工具自行解析

    static {
        String resourceName = "config-" + active + ".properties";
        // 不会自动解析 ${} 中的变量
        InputStream resourceAsStream = PropertiesDemo.class.getClassLoader().getResourceAsStream(resourceName);
        try {
            prop.load(resourceAsStream);
            resourceAsStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        rootDir = prop.getProperty("root.dir");
        userDir = prop.getProperty("user.dir");
        username = prop.getProperty("username");
    }

    public static String getString(String key) {
        return prop.getProperty(key);
    }

    public static void main(String[] args) {
        System.out.println(rootDir);
        System.out.println(userDir);
        System.out.println(username);
    }
}
