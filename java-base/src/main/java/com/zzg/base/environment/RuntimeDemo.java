package com.zzg.base.environment;


import java.io.IOException;

/**
 * https://blog.csdn.net/weixin_45433031/article/details/112209263
 * 当前类放在 environment 包下进行操作，但是并不是环境变量的一部分，只是可以获取一些系统属性
 *
 * 每个Java应用程序都有一个Runtime类的Runtime ，允许应用程序与运行应用程序的环境进行接口。 当前运行时可以从getRuntime方法获得。
 * 应用程序无法创建自己的此类的实例。 是一个 单例设计模式
 *
 * 一般不能实例化一个Runtime对象，应用程序也不能创建自己的 Runtime 类实例，但可以通过 getRuntime 方法获取当前Runtime运行时对象的引用。
 * 一旦得到了一个当前的Runtime对象的引用，就可以调用Runtime对象的方法去控制Java虚拟机的状态和行为。
 * 当Applet和其他不被信任的代码调用任何Runtime方法时，常常会引起SecurityException异常。
 */
public class RuntimeDemo {

    public static void main(String[] args) throws IOException {
        Runtime runtime = Runtime.getRuntime();

        // 返回可用于Java虚拟机的处理器数量。
        System.out.println("runtime.availableProcessors() = " + runtime.availableProcessors());

        // 返回Java虚拟机中的可用内存量。 - 单位B   res / 1024 / 1024 换算M
        System.out.println("runtime.freeMemory() = " + runtime.freeMemory());

        // 返回Java虚拟机将尝试使用的最大内存量。
        System.out.println("runtime.maxMemory() = " + runtime.maxMemory());

        // 返回Java虚拟机中的内存总量。
        System.out.println("runtime.totalMemory() = " + runtime.totalMemory());

        // 在单独的进程中执行指定的字符串命令。 可以用于执行 CMD 命令或者linux下的 sh 命令
        runtime.exec("notepad.exe");    //打开记事本。必须在window环境下运行
    }

}
