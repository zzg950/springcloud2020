package com.zzg.base.plugins.lombok.example;

import lombok.Value;

/**
 * @Value 注解 - 不推荐
 * 将一个类变的不可变，不能被继承、类中的属性也不能被修改。
 *
 * 会使得类变成 final 的
 * 会使得没有声明访问权限的属性变成私有的
 * 会使得属性变为 final 的，可以通过 @NonFinal 来标记某个属性不变为 final
 * 同时还会生成 getter、 setter、 equals()、 hashCode()、 toString() 方法
 * 还会生成一个全字段的构造方法
 * @getter + @setter + @EqualsAndHashCode + @ToString + @AllArgsConstructor
 */

@Value
public class ValueExample {

    public static void main(String[] args) {

    }

}
