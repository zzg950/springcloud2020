

## Lombok
人生苦短，我用Lombok。

## Maven 引入
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.20</version>
</dependency>

## Jar 包引入
如果不是Maven工程，则只需要将对应的jar包加入到工程里面即可。
1. 将 lombok jar 包放到项目的 lib 目录下，然后在项目中引入 lib 目录中的 jar 包。
2. 将 lombok jar 包放到 JDK 的 ext 目录下，例如：C:\Program Files\Java\jdk1.8.0_152\jre\lib\ext。

同时还需要在开发工具中引入 lombok 的插件。

