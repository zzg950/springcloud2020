package com.zzg.base.plugins.lombok.example;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;


/**
 * @NonNull 表示注解所修饰的字段或者参数不能为空
 * 1. 注解在字段上面，表示此字段不能为空，创建对象后给对应的字段set值时，如果是空，则报错
 * 2. 注解在参数上面，表示调用次方法赋值必然不能为空，否则报错
 */
@Getter
@Setter
@ToString
public class NonNullExample {

    @NonNull
    private String name;
    private String address;
    private int age = 15;
    private Date birthday;


    // @NonNull 注解定义在方法的参数上面，表示此方法参数不能为null，否则运行时将报错
    public void example(@NonNull String line) {
        System.out.println(line);
    }


    public static void main(String[] args) {
        NonNullExample example = new NonNullExample();
        example.setAddress("中国");
//        example.setName(null);  // 报错
        System.out.println("example = " + example);

//        example.example(null);  // 报错
    }

}
