package com.zzg.base.plugins.lombok;


import com.zzg.springcloud.common.util.DateUtils;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;

import java.io.*;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"status", "age"}, of = {"name"})
@EqualsAndHashCode
@FieldNameConstants
public class LombokEntity {

    private String name;

    private String password;
    private int status;
    private int age = 15;
    private Date rDate;

    @Singular("addHobby")
    private List<String> hobbies;


}
