package com.zzg.base.plugins.lombok.example;

import lombok.*;

import java.util.Date;
import java.util.List;


/**
 * 使用@Builder会自动创建链式调用方法对对象赋值
 * 1. 默认创建所有参数的构造方法，所以如果需要配合 @NoArgsConstructor 使用，否则没有无参构造方法
 * 2. 如果需要给对象设置初始默认值，则需要在字段上增加注解 @Builder.Default 否则使用链式创建的对象，默认值不生效
 * 3. 可配合 @Singular 注解，对集合属性进行追加赋值
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BuilderExample {

    private String name;
    private String address;
    @Builder.Default
    private int age = 15;
    private Date birthday;

    @Singular("addHobby")
    private List<String> hobbies;


    public static void main(String[] args) {
        BuilderExample example = BuilderExample.builder().name("张志刚")
                .addHobby("read").addHobby("drink").build();
        System.out.println(example);
    }

}
