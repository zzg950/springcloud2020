package com.zzg.base.plugins.lombok.example;


//@Accessors(chain = true)  // 对set方法进行特殊处理，返回的不是void，而是当前对象，注解后可以使 set 方法连续调用
//@Accessors(fluent = true) // 不推荐使用，使用后取消get和set方法的前缀，直接使用对象的属性进行取值和赋值，乱搞一气
public class AccessorsExample {
}
