package com.zzg.base.plugins.lombok;


public class LombokTest {

    public static void main(String[] args) throws Exception {

        LombokEntity entity = new LombokEntity();
        entity.setName("张志刚");
        entity.setAge(15);
//        entity.example(null);
//         @Accessors(chain = true)
        // 使用上面注解后，所有的 setter 方法返回的是当前对象，所以可以一直通过使用set的方法相当于链式赋值
        // 缺点就是 setter 方法返回的不是 void 不知道有没有其他影响
//        entity.setName("张志刚").setPassword("121212");

        System.out.println("entity.getName() = " + entity.getName());
        System.out.println(entity);

        // @Builder 链式调用
        LombokEntity entity1 = LombokEntity.builder().name("张志刚Builder").age(11).addHobby("read").build();
        System.out.println(entity1);

        System.out.println("LombokEntity.Fields.age = " + LombokEntity.Fields.age);
        System.out.println("LombokEntity.Fields.name = " + LombokEntity.Fields.name);

    }

}
