
## 说明
Guava 是谷歌开源的 Java 核心库，它包括很多新的集合类型（如 multimap 和 multiset）、不可变集合、graph 库，
以及用于并发、I/O、哈希、缓存、基本类型、字符串等的实用代码！它被广泛应用于谷歌中的大多数 Java 项目中，也被许多其他公司广泛使用。

## Maven 引入

<dependency>
  <groupId>com.google.guava</groupId>
  <artifactId>guava</artifactId>
  <version>31.1-jre</version>
</dependency>

