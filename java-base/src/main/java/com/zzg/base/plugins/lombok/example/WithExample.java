package com.zzg.base.plugins.lombok.example;

import lombok.*;

import java.util.Date;

/**
 * @With 注解表示如果属性发生变动，则返回一个全新的对象
 * 1. 注解在 Class 上面，则给每一个字段增加一个 with 方法，如果设置的参数发生变化，则重新创建一个新的对象
 * 2. 注解在 Field 上面，则给对应的字段的 setter 方法进行重写，如果setter的值发生变化，则重新创建一个新的对象返回
 */
@Setter
@NoArgsConstructor
@AllArgsConstructor
@With
public class WithExample {

    private String name;
    private String address;
    private int age = 15;
    private Date birthday;

    public static void main(String[] args) {
        WithExample withExample = new WithExample();
        withExample.setAddress("华夏");
        System.out.println("对象内存地址1 --> " + withExample);
        withExample.setAddress("中国");
        System.out.println("set方法后对象地址不变 --> " + withExample);
        withExample = withExample.withAddress("中华人民共和国");
        System.out.println("with方法后对象地址变化 --> " + withExample);
    }


    /** 编译后的 with 方法如下
    public WithExample withAddress(String address) {
        return this.address == address ? this : new WithExample(this.name, address, this.age, this.birthday);
    }
    */
}
