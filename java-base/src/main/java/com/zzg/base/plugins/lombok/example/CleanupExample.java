package com.zzg.base.plugins.lombok.example;


import com.zzg.springcloud.common.util.DateUtils;
import lombok.Cleanup;

import java.io.*;

/**
 * @Cleanup 注解使用在本地变量上面
 * 1. 可以自动调用文件流的 close 方法
 */

public class CleanupExample {

    // 使用注解写法，不用手动关闭文件流
    public void copyFile() throws IOException {
        long start = System.currentTimeMillis();
        @Cleanup InputStream fis = new FileInputStream("D:\\myapps\\spark-2.3.0-bin-hadoop2.7.tgz");
        @Cleanup OutputStream fos = new FileOutputStream("D:\\myapps\\spark-2.3.0-bin-hadoop2.7_" + DateUtils.getDate(DateUtils.yyyyMMddHHmmss) + ".tgz");

        int len = 1024 * 1024;
        byte[] bytes = new byte[len];
        while (true) {
            int r = fis.read(bytes);
            if (r <= -1) break;
            fos.write(bytes);
        }
        System.out.println("复制管道大小 : len = " + len + "; 耗时 : cost = " + (System.currentTimeMillis() - start));
    }

    // 使用 try-with-resource 写法 - 不用手动关闭文件流
    public void copyFile2() {
        try (InputStream fis = new FileInputStream("D:\\myapps\\spark-2.3.0-bin-hadoop2.7.tgz");
             OutputStream fos = new FileOutputStream("D:\\myapps\\spark-2.3.0-bin-hadoop2.7_" + DateUtils.getDate(DateUtils.yyyyMMddHHmmss) + ".tgz");
        ){
            byte[] bytes = new byte[1024];
            while (true) {
                int r = fis.read(bytes);
                if (r <= -1) break;
                fos.write(bytes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        CleanupExample example = new CleanupExample();
        example.copyFile();
    }

}
