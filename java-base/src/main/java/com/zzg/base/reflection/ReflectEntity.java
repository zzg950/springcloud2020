package com.zzg.base.reflection;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

/**
 * 用来测试反射机制的实体类
 * 1. 包含私有属性 - 提供get方法和不提供get方法的
 * 2. 包含静态属性
 * 3. 包含构造方法 - 私有构造方法和公共构造方法
 * 4. 包含普通方法 - 私有方法和公共方法
 */
public class ReflectEntity extends ParentReflectEntity {

    /**
     * ReflectEntity的内部类
     * ReflectEntity.InnerClass.class 通过此种方法可以获取
     * <p>
     * 期望使用 new ReflectEntity.InnerClass(); 来创建内部类对象，需要使用 static class InnerClass 修饰， IDE 推荐的使用 static 修饰
     * ReflectEntity.InnerClass innerClass = new ReflectEntity.InnerClass();
     */
    static class InnerClass {
        private String inner_field_private;
        public String inner_field_public;

        public InnerClass() {
        }

        public InnerClass(String inner_field_private) {
            this.inner_field_private = inner_field_private;
        }

        public String getInner_field_private() {
            return inner_field_private;
        }

        public void setInner_field_private(String inner_field_private) {
            this.inner_field_private = inner_field_private;
        }
    }

    // 静态代码块 - 在类被加载时执行一次 - 在项目的整个生命周期中只执行一次
    static {
        System.out.println("~~~~~~~~~ init static code ~~~~~~~~~");
    }

    /**
     * 定义一个私有、静态、final的对象 - 正常情况下只能在当前类中使用
     */
    private static final Logger logger = Logger.getLogger(ReflectEntity.class.getName());

    private transient String transient_field;

    public volatile int volatile_filed;

    /**
     * 定一个私有、静态的内部变量 - 正常情况下外部是无法获取此对象值的，也无法修改此值的
     */
    private static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public final static String CHARSET_NAME = "UTF-8";
    public List<String> stringList = new ArrayList<>();

    protected final static Integer protected_final_static_field = 123;
    protected static String protected_static_field;

    final static List<String> default_final_static_field = Arrays.asList("1,2,3,4,5,6,7,9".split(","));
    StringJoiner default_field;

    private DataSource dataSource;
    private Integer age;
    private String name;
    private Date birthday;

    /**
     * 提供公共无参构造方法
     */
    public ReflectEntity() {
    }

    /**
     * 由于无参构造方法在测试中需要经过修改其修饰符
     * 定义一个一个参数的构造方法，用于创建对象进行测试
     */
    public ReflectEntity(String name) {
        this.name = name;
    }

    /**
     * 提供私有有参构造方法
     */
    private ReflectEntity(Integer age, String name) {
        this.age = age;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    // 提供私有方法 - 正常情况下只能内部使用
    private String formatBirthday() {
        if (birthday == null) return null;
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        return sdf.format(birthday);
    }

    // 静态私有方法
    private static String staticPrivateMethod() {
        return "This is static private method.";
    }

    private static String staticPrivateMethod(String param) {
        return "This is static private method param = " + param;
    }

    protected void protectedMethod() {
    }

    protected static void protectedStaticMethod() {
    }

    void defaultMethod() {
    }

    static void defaultStaticMethod() {
    }

    // 提供公共方法
    public int add(int a, int b) {
        return a + b;
    }

    // 静态方法
    public static int randomInt() {
        return new Random().nextInt();
    }

    public static int randomInt(int bound) {
        return new Random().nextInt(bound);
    }


    @Override
    public String toString() {
        return String.format("[ReflectEntity : logger = %s; DEFAULT_DATE_FORMAT = %s; dataSource = %s; age = %s; name = %s; birthday = %s;]",
                logger, DEFAULT_DATE_FORMAT, dataSource, age, name, formatBirthday());
    }

}
