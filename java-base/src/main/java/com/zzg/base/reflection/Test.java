package com.zzg.base.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<String> li = new ArrayList<>();
        li.add("牛逼");
        //获取当前对象的class
        Class cla = li.getClass();
        //获取方法
        Method m = cla.getMethod("add", Object.class);
        //调用方法
        m.invoke(li, 11);
        for (Object obj : li) {
            System.out.println(obj instanceof Number);
        }

        Method get = cla.getMethod("get", int.class);
        Object res = get.invoke(li, 1);
        System.out.println(res);

    }
}
