package com.zzg.base.instance;

import com.zzg.base.entity.Animal;
import com.zzg.base.entity.Cat;
import com.zzg.base.entity.Dog;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 使用 instanceof 关键字，可以用于判断数据类型 - 只要互相之间有继承关系即可返回true
 * 1. 数据类型跟 instanceof 后面的类型一致，返回true
 * 2. 数据类型是 instanceof 后面的子类型，返回true
 * 3. 数据类型是 instanceof 后面的父类型，返回true
 * 4. 比较的对象是泛型类型数据的话，忽略泛型内容进行比较
 * <p>
 * 例如: A extends B; B extends C;
 * <p>
 * a instanceof A : true
 * b instanceof B : true
 * a instanceof B : true
 * b instanceof A : true
 * <p>
 * 后续不想写了，以后再说 - 20220506 - 了解上面结论即可
 * <p>
 * 还可以使用 A.class.isInstance(a) 这种方式来判断 - 后续更新
 */
public class InstanceofDemo {

    public static void main(String[] args) {
//        baseDataInstanceMethod();
//        System.out.println("==================================");
//        listInstanceMethod();
        System.out.println("===================================");
        objectInstanceMethod();
    }


    /**
     * 查看各类对象之间是否有关系
     * 1. Animal implements IAnimal
     * 2. Cat extends Animal
     * 3. Dog extends Animal
     */
    public static void objectInstanceMethod() {
        Object animal = new Animal() {
            @Override
            public void eat(String food) {
                System.out.println("Animal like any food; include " + food);
            }
        };

        Animal cat = new Cat();
        Object catObject = new Cat();
        Object dogObject = new Dog();

        // 子类 instanceof 父类 -- true
        System.out.println("(cat instanceof Animal) = " + (cat instanceof Animal));                 // true
        System.out.println("(catObject instanceof Animal) = " + (catObject instanceof Animal));     // true
        System.out.println("(dogObject instanceof Animal) = " + (dogObject instanceof Animal));     // true

        // 父类 instanceof 子类 -- false
        System.out.println("(animal instanceof Cat) = " + (animal instanceof Cat));
        System.out.println("(animal instanceof Dog) = " + (animal instanceof Dog));

        // 一个父类的两个子类相互 instanceof -- false
        System.out.println("(cat instanceof Dog) = " + (cat instanceof Dog));
        System.out.println("(catObject instanceof Dog) = " + (catObject instanceof Dog));

    }


    /**
     * 对基础数据类型的判断
     * Integer 和 Long 都继承自 Number 对象
     */
    public static void baseDataInstanceMethod() {
        // 数据类型一致的比较
        Number number = 123;
        Number numberLong = 987L;
        Integer integer = 234;
        Long time = System.currentTimeMillis();

        // 父类 instanceof 子类
        System.out.println("Number instanceof Integer = " + (number instanceof Integer));       // true
        System.out.println("(number instanceof Long) = " + (number instanceof Long));
        System.out.println("(numberLong instanceof Integer) = " + (numberLong instanceof Integer));
        System.out.println("(numberLong instanceof Long) = " + (numberLong instanceof Long));

        // 子类 instanceof 父类
        System.out.println("Integer instanceof Number = " + (integer instanceof Number));   // true
        System.out.println("Long instanceof Number = " + (time instanceof Number));         // true


//        System.out.println("Long instanceof Integer = " + (time instanceof Integer));  // 编译不通过 - 父类型
    }


    /**
     * 对集合类型的判断
     * 1. 忽略泛型的
     */
    public static void listInstanceMethod() {
        List<String> strList = new ArrayList<>();
        List<Integer> intList = new ArrayList<>();
        List<String> strlinkedList = new LinkedList<>();
        System.out.println("strList instanceof List = " + (strList instanceof List));
        System.out.println("strList instanceof ArrayList = " + (strlinkedList instanceof ArrayList));
        System.out.println("strList instanceof LinkedList = " + (intList instanceof LinkedList));

        System.out.println("strList.getClass() == intList.getClass() = " + (strList.getClass() == intList.getClass()));
        System.out.println("strList.getClass() == strlinkedList.getClass() = " + (strList.getClass() == strlinkedList.getClass()));
        System.out.println("intList.getClass() = " + intList.getClass());

    }

    public static void compareMethod() {
        String str = "123";
        String str2 = "张志刚";
        Object obj = "张志刚2";
        System.out.println(str.getClass() == str2.getClass());
        System.out.println(str.getClass() == obj.getClass());
        System.out.println("str.getClass() == String.class = " + (str.getClass() == String.class));
    }
}
