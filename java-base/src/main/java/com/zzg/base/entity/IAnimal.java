package com.zzg.base.entity;

/**
 * https://blog.csdn.net/m0_52226803/article/details/119567669
 * 1. java中接口的方法默认是 public  abstract 的
 * 2. 所以放心的删掉public即可，如果改为protected 或者 private还会报错
 */
public interface IAnimal {

    public abstract void eat(String food);

    public String printClassAndName();

}
