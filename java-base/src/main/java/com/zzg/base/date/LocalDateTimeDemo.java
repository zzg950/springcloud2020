package com.zzg.base.date;


import sun.util.resources.LocaleData;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * LocalDateTime = LocalDate + LocalTime
 * 1. 默认格式 2022-05-02T23:49:24.800
 * 2. 其他API跟LocalDate和LocalTime一样
 *
 * DateTimeFormatter 和 SimpleDateFormat 都是用于格式化和解析日期时间的类，但是它们有几个重要的区别：
 * 线程安全性：DateTimeFormatter 是线程安全的，而 SimpleDateFormat 是非线程安全的。
 *  可扩展性：DateTimeFormatter 是可扩展的，支持更多可自定义的日期时间格式，而 SimpleDateFormat 仅支持固定的日期时间格式模式。
 *  设置时区：DateTimeFormatter 可以设置时区，而 SimpleDateFormat 需要在格式化或解析之前手动设置时区。
 *  解析方式：DateTimeFormatter 使用更严格的解析方式，不容忍错误的日期时间格式，而 SimpleDateFormat 对于错误格式的处理比较宽松。
 * 总的来说，建议在多线程环境下使用 DateTimeFormatter，因为它是线程安全的，而且具有更好的可扩展性和更严格的解析方式。
 * 如果在单线程环境下使用，可以选择使用 SimpleDateFormat，因为它的格式化和解析速度比较快，并且用起来比较方便。
 */
public class LocalDateTimeDemo {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println("localDate = " + localDate);
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("localDateTime = " + localDateTime);
        System.out.println("==========================");

        String resultFormat = "yyyy-MM-dd HH:mm:ss";
        DateTimeFormatter resultDateFormatter = DateTimeFormatter.ofPattern(resultFormat);
        SimpleDateFormat resultSimpleFormat = new SimpleDateFormat(resultFormat);

        /**
         * 比较 DateTimeFormatter 和 SimpleDateFormat 解析字符串的严谨性
         */
        String format = "yyyyMMdd";
        String dateString = "20230505";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(format);
        try {
            System.out.println(resultDateFormatter.format(dateFormatter.parse(dateString)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
        simpleFormat.setLenient(false);
        try {
            System.out.println(resultSimpleFormat.format(simpleFormat.parse(dateString)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
            System.out.println(dateTimeFormatter.parse("20230511"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
