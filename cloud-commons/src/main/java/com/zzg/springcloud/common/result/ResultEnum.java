package com.zzg.springcloud.common.result;

/**
 * @author zzg
 * 枚举类型的定义和使用
 */
public enum ResultEnum {

    SUCCESS(0, "SUCCESS"),
    UNKONW_ERROR(1, "未知异常"),
    USER_NOTEXIST(2, "用户不存在或密码错误"),
    USER_REGISTER_FAIL(3, "注册失败"),

    FIELD_SAVEFAIL(4, "数据字段保存失败"),
    FIELD_FREQUENTLY(5, "请求过于频繁"),
    FIELD_BEYOND(6, "本渠道该地区下可用case以达最大限制"),
    FIELD_NOTLOGIN(7, "您未登陆，请先登陆"),
    FIELD_AUTHERROR(8, "您的权限不足，请联系管理员"),
    REPEAT_SUBMIT(10, "重复提交"),
    REPEAT_NOTFIND(11, "未查到数据"),
    CASE_NOTFIND(12, "未查到可用用例信息"),
    USER_PASSWORDREPEAT(13, "新密码与旧密码相同"),

    POLICYNO_NOT_EXIST(13, "保单号不存在"),
    POLICYNO_NOT_EXIST_JQX(131, "交强险保单号不存在"),
    POLICYNO_NOT_EXIST_SYX(132, "商业险保单号不存在");

    /**
     * 定义枚举类型的字段
     */
    private Integer code;
    private String message;

    /**
     * 枚举构造函数不允许使用public或protected修饰符。 - 枚举构造函数是用来定义内部常量的，而不是创建和初始化枚举的不同实例，所以必须是私有的
     * Modifier private is redundant for enum constructor. - 私有修饰符对于枚举的构造方法是多余的
     */
    private ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static ResultEnum get(Integer code) {
        ResultEnum[] values = ResultEnum.values();
        for (ResultEnum element : values) {
            if (code == element.getCode()) {
                return element;
            }
        }
        return null;
    }


    public static void main(String[] args) {
        // Enum提供的toString方法
        System.out.println("ResultEnum.SUCCESS.toString()  = " + ResultEnum.SUCCESS.toString());
        System.out.println("ResultEnum.SUCCESS.name() = " + ResultEnum.SUCCESS.name());

        // 获取枚举的 code 和 message
        System.out.printf("ResultEnum.SUCCESS.getCode()    = %10s %n", ResultEnum.SUCCESS.getCode());
        System.out.printf("ResultEnum.SUCCESS.getMessage() = %10s %n", ResultEnum.SUCCESS.getMessage());

        // Enum提供了一个valueOf方法, 是根据枚举值的字符串返回指定的枚举对象
        System.out.println(ResultEnum.valueOf("SUCCESS"));

        // 自定义的get方法，根据枚举的code获取枚举对象
        System.out.println(ResultEnum.get(1));

        // getCode 和 getMessage方法
        System.out.println("ResultEnum.get(1).getCode() = " + ResultEnum.get(1).getCode());
        System.out.println("ResultEnum.get(1).getMessage() = " + ResultEnum.get(1).getMessage());

    }

}
