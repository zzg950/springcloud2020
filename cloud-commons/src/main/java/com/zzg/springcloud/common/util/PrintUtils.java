package com.zzg.springcloud.common.util;


import java.util.StringJoiner;

/**
 * 打印工具类
 * 1. 增加打印线程的内容
 */
public class PrintUtils {

    public static void printThreadAndMessage(Object message) {
        String printMessage = "";
        if (message != null) printMessage = message.toString();
        String res = new StringJoiner("\t|\t")
                .add(String.valueOf(Thread.currentThread().getId()))
                .add(Thread.currentThread().getName())
                .add(printMessage).toString();
        System.out.println(res);
    }

    public static void format(String format, Object... objs) {
        System.out.println(String.format(format, objs));
    }

    public static void main(String[] args) {
        PrintUtils.format("index = %d; message = %40s;", 100, "ERROR");
    }

}
