package com.zzg.springcloud.common.result;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

public class ReturnResult {

    private int code;
    private String message;
    private Object data;

    /**
     * 提供多种构造方法
     */
    public ReturnResult() {
        super();
    }

    public ReturnResult(int code) {
        super();
        this.code = code;
    }

    public ReturnResult(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public ReturnResult(int code, String message, Object data) {
        super();
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ReturnResult(ResultEnum resultEnum, Object data) {
        super();
        this.code = resultEnum.getCode();
        this.message = resultEnum.getMessage();
        this.data = data;
    }

    public ReturnResult(ResultEnum envm) {
        super();
        this.code = envm.getCode();
        this.message = envm.getMessage();
    }

    /**
     * getter/setter方法
     */
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 2022-05-04
     * 新增泛型返回之后，取消当前方法
     * public Object getData() {
     * return data;
     * }
     */

    public void setData(Object data) {
        this.data = data;
    }

    /**
     * @author 张志刚  2018年7月16日 下午10:27:44
     * @功能 :  定义一批静态方法提供使用
     */
    public static ReturnResult get() {
        return new ReturnResult(ResultEnum.SUCCESS);
    }

    public static ReturnResult get(Object data) {
        return new ReturnResult(ResultEnum.SUCCESS, data);
    }

    public static ReturnResult getSuccessResult() {
        return new ReturnResult(ResultEnum.SUCCESS);
    }

    public static ReturnResult getSuccessResult(Object data) {
        return new ReturnResult(ResultEnum.SUCCESS, data);
    }


    public static ReturnResult getErrorResult() {
        return new ReturnResult(ResultEnum.UNKONW_ERROR);
    }

    public static ReturnResult getErrorResult(String errorMessage) {
        return new ReturnResult(ResultEnum.UNKONW_ERROR.getCode(), errorMessage);
    }

    public static ReturnResult getErrorResult(ResultEnum resultEnum) {
        return new ReturnResult(resultEnum);
    }

    public static ReturnResult getErrorResult(ResultEnum resultEnum, Object data) {
        return new ReturnResult(resultEnum, data);
    }

    /**
     * <p>
     * Title : 通过临时定义编码和提示语的方式返回数据集
     * </p>
     * <p>
     * Description : 扩展方法 - 不推荐使用
     * </p>
     *
     * @param errorCode
     * @param errorMessage
     * @author 张志刚  2018年7月16日 下午10:38:47
     */
    public static ReturnResult getErrorResult(int errorCode, String errorMessage) {
        return new ReturnResult(errorCode, errorMessage);
    }

    public static ReturnResult getErrorResult(int errorCode, String errorMessage, Object data) {
        return new ReturnResult(errorCode, errorMessage, data);
    }

    /**
     * 根据 class 将 data 值进行强转后返回
     */
    public <T> List<T> getList(Class<T> clazz) {
        String json = JSONObject.toJSONString(data);
        if (json == null || json == "") return null;
        return JSONArray.parseArray(json, clazz);
    }

    public <T> T getData(Class<T> clazz) {
        String json = JSONObject.toJSONString(data);
        if (json == null || json == "") return null;
        return (T) JSON.parseObject(json, clazz);
    }

    /**
     * 2022-05-04
     * 学习泛型新功能之后新增方法 - 不需要传递Class参数，直接根据接收值的类型返回
     * 在接受的时候声明了类型，那么就可以对返回值进行强制转换，然后返回
     * 当然，如果类型不匹配的话，会抛出运行时错误，强制转换失败 - java.lang.ClassCastException
     */
    public <T> T getData() {
        if (data == null) return null;
        return (T) data;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
