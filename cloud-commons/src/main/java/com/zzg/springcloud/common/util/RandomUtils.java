package com.zzg.springcloud.common.util;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;

import java.util.Random;

/**
 * 生成短信验证码和随机密码工具类
 */
public class RandomUtils {

    private static String getRandomNumString() {
        double d = Math.random();
        String dStr = String.valueOf(d).replaceAll("[^\\d]", "");
        if (dStr.length() > 1)
            dStr = dStr.substring(0, dStr.length() - 1);
        return dStr;
    }

    private static int getInt() {
        int ret = Math.abs(Long.valueOf(getRandomNumString()).intValue());
        return ret;
    }

    /**
     * random int, scope: 0 ~ (mod-1)
     *
     * @param mod scope
     * @return
     */
    public static int getRandom(int mod) {
        if (mod < 1) return 0;
        return getInt() % mod;
    }

    /**
     * @param min 最小值，包括
     * @param max 最大值，不包括
     * @return
     */
    public static int getRandom(int min, int max) {
        return RandomUtil.randomInt(min, max);
    }


    /**
     * 随机生成字符串
     *
     * @param length 字符串长度
     * @return
     */
    public static String randomString(int length) {
        String str = null;
        if (length <= 0)
            return null;
        String charset = "abcdefghijklmnopqrstuvwxyz1234567890!#$@%&*-=+|/ABCDEFGHIJKLMNOQPRSTUVWXYZ";
        Random r = new Random();
        Random r1 = new Random();
        StringBuffer bf = new StringBuffer();
        int ba = Math.abs(r1.nextInt() % length) + 1;
        for (int i = 0; i < ba; i++) {
            int radix = Math.abs(r.nextInt(ba) % charset.length());
            char c = charset.charAt(radix);
            bf.append(c);
        }

        str = bf.toString();
        return str;
    }


    static char[] codeSequence = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    /**
     * 生成4位验证码
     *
     * @return
     * @Title: generateCode4
     * @Description:
     */
    public static String generateCode4() {
        Random random = new Random();
        StringBuffer randomCode = new StringBuffer();
        for (int i = 0; i < 4; i++) {
            randomCode.append(random.nextInt(10));
        }
        return randomCode.toString();
    }

    /**
     * 生成6位验证码
     *
     * @return
     * @Title: generateCode
     * @Description:
     */
    public static String generateCode6() {
        Random random = new Random();
        StringBuffer randomCode = new StringBuffer();
        for (int i = 0; i < 6; i++) {
            randomCode.append(random.nextInt(10));
        }
        return randomCode.toString();
    }

    /**
     * 生成6位随机密码
     *
     * @return
     * @Title: generatePass
     * @Description:
     */
    public static String generatePass6() {
        Random random = new Random();
        StringBuffer randomCode = new StringBuffer();
        int digist = 0;
        for (int i = 0; i < 6; i++) {
            char a = codeSequence[random.nextInt(36)];
            if (Character.isDigit(a)) {
                digist++;
            }
            randomCode.append(a);
        }
        // 去掉全部数字或全部字母
        if (digist == 0 || digist == 6) {
            return generatePass6();
        }
        return randomCode.toString();
    }


    // private static final String charlist = "0123456789";
    public static final int inc = 1000;
    public static final int len = 16;
    public static final String[] charlistArray = new String[]{"0123456789", "5439768210", "1783054962", "3407981526",
            "2845639701"};

    /**
     * 生成随机字符串(random string, but it is not unique)
     *
     * @param len 随机字符串长度
     * @return random string
     */
    public static String createRandomString(int len) {

        String charlist = charlistArray[(int) (getInt() % 5)];
        String str = new String();
        for (int i = 0; i < len; i++)
            str += charlist.charAt(getRandom(charlist.length()));
        return str;
    }

    /**
     * 生成三位随机数
     *
     * @return
     */
    public static String getRandomNum() {
        //三位是+100,如果是四位的话改成1000,以此类推
//		Random random = new Random();
//		nextInt取大于等于0小于n的随机数
//		int num = random.nextInt(900) + 100;
        // Math.random()取大于等于0.0、小于1.0的double型随机数
        int num = (int) ((Math.random() * 9 + 1) * 100);
        return String.valueOf(num);
    }

    /**
     * 生成随机数
     *
     * @param length
     * @return
     */
    public static String getRandomCode(int length) {
        String verCode = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            verCode += random.nextInt(10);
        }
        return verCode;
    }

    public static void main(String[] args) {
        System.out.println("getInt() = " + getInt());
        System.out.println("getRandomNumString() = " + getRandomNumString());
        System.out.println("Math.random() = " + Math.random());
        System.out.println("getRandomNum() = " + getRandomNum());
        System.out.println("getRandomCode(3) = " + getRandomCode(3));
        System.out.println((int) ((Math.random() * 9 + 1) * 100));
        Random random = new Random();
        System.out.println("random.nextInt() = " + random.nextInt());
        System.out.println("random.nextInt(10) = " + random.nextInt(10));
        System.out.println(random.nextInt(900) + 100);
        System.out.println("*********");

//		for (int i = 0; i < 100; i++) {
//			System.out.println(generatePass6());
//		}

    }
}
