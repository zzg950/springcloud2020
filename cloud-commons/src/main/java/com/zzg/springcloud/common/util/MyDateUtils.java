package com.zzg.springcloud.common.util;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 2023-04-13 从研究生系统提炼的 DateUtils，除去普通的方法外，有额外几个方法
 * 1. 日期字符串自动转日期对象 - 根据字符串自动判断所属
 * 2. 日期字符串格式互转
 */
public class MyDateUtils {

    // 预置的可以支持的日期格式
    public static class Pattern {
        public static final String DEFAULT_FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";
        public static final String DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
        public static final String CHINESE_FORMAT_YYYY_M = "yyyy年M月";
        public static final String ENGLISH_PRINT_FORMAT = "MMM. d, yyyy";
        public static final String CHINESE_PRINT_FORMAT = "yyyy年M月d日";
        public static final String yyyy = "yyyy";
        public static final String MM = "MM";
        public static final String dd = "dd";
        public static final String yyyyMM = "yyyyMM";
        public static final String yyyyMMdd = "yyyyMMdd";
        public static final String yyyyMMddHH = "yyyyMMddHH";
        public static final String yyyyMMddHHmm = "yyyyMMddHHmm";
        public static final String yyyyMMddHHmmss = "yyyyMMddHHmmss";
        public static final String yyyyMMddHHmmssSSS = "yyyyMMddHHmmssSSS";
    }


    /**
     * 定义一些long类型的时间毫秒值
     */
    public static class Unit {
        public static final long MINUTE = 60 * 1000L;
        public static final long HOUR = MINUTE * 60;
        public static final long DAY = HOUR * 24;
        public static final long WEEK = DAY * 7;
    }

    /**
     * =============================================时间格式部分===========================开始=========================
     * format 方法是最基本的方法，后面所有时间格式都是基于此方法进行处理
     * format_xxxx : xxxx 如果是date则表示默认的格式是 : yyyy-MM-dd; xxxx 如果是datetime则表示默认的格式是 yyyy-MM-dd HH:mm:ss
     */
    public static String format(Date date, String format, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        return sdf.format(date);
    }

    public static String format(Date date, String format) {
        return format(date, format, Locale.CHINA);
    }

    public static String format(String format) {
        return format(new Date(), format);
    }


    public static String formatDate() {
        return format(new Date(), Pattern.DEFAULT_FORMAT_DATE);
    }

    public static String formatDate(Date date) {
        return format(date, Pattern.DEFAULT_FORMAT_DATE);
    }

    public static String formatDate(long millisecond) {
        return format(new Date(millisecond), Pattern.DEFAULT_FORMAT_DATE);
    }

    public static String formatDatetime() {
        return format(new Date(), Pattern.DEFAULT_FORMAT_DATETIME);
    }

    public static String formatDatetime(Date date) {
        return format(date, Pattern.DEFAULT_FORMAT_DATETIME);
    }

    public static String formatDatetime(long millisecond) {
        return format(new Date(millisecond), Pattern.DEFAULT_FORMAT_DATETIME);
    }

    /**
     * 在打印或者下载的文件中，所有的日期格式尽量保持统一
     * 中文的要求是 : 2023年3月25日
     * 英文的要求是 : Mar. 25, 2023
     */
    // 获取中文的打印日期: 默认当前日期，格式是 2023年3月25日
    public static String getChinesePrintDate() {
        return getChinesePrintDate(new Date());
    }
    public static String getChinesePrintDate(Date date) {
        return format(date, Pattern.CHINESE_PRINT_FORMAT);
    }

    // 获取英文的打印日期: 默认当前日期， 格式是 Mar. 25, 2023
    public static String getEnglishPrintDate() {
        return getEnglishPrintDate(new Date());
    }
    public static String getEnglishPrintDate(Date date) {
        return format(new Date(), Pattern.ENGLISH_PRINT_FORMAT, Locale.ENGLISH);
    }

    /**
     * 获取日期，中文大写格式
     * 20211101 - 二〇二一年十一月一日
     */
    public static String getDateChinese(String dateStr, String dateFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            Date date = sdf.parse(dateStr);
            sdf = new SimpleDateFormat(Pattern.DEFAULT_FORMAT_DATE);
            String transRes = sdf.format(date);
            StringBuilder res = new StringBuilder();
            String[] array = transRes.split("-");
            String[] yyyy = array[0].split("");
            for (String y : yyyy) {
                res.append(getChineseByte(y));
            }
            res.append("年");
            res.append(getChineseByte(array[1])).append("月");
            res.append(getChineseByte(array[2])).append("日");
            return res.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String getChineseByte(String num) {
//		String[] nums = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31".split(",");
        String[] chinese = "〇,一,二,三,四,五,六,七,八,九,十,十一,十二,十三,十四,十五,十六,十七,十八,十九,二十,二十一,二十二,二十三,二十四,二十五,二十六,二十七,二十八,二十九,三十,三十一".split(",");
        int index = Integer.parseInt(num);
        return index > chinese.length ? "" : chinese[index];
    }

    /**
     * 时间字符串转日期
     * @param value 待转换字符串
     * @return Date 若入参为空则返回null
     */
    public static Date parse(String value) {
        String pattern = guessPattern(value);
        if (StringUtils.isEmpty(pattern)) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            return simpleDateFormat.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 字符串转时间
     * 默认格式yyyy-MM-dd HH:mm:ss
     * @param date
     * @return
     */
    public static Date parseDate(String date) {
        return parse(date);
    }

    /**
     * 字符串转时间
     * @param date
     * @param format 格式，比如yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date parseDate(String date, String format) {
        try {
            DateFormat df = new SimpleDateFormat(format);
            return df.parse(date);
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * 校验指定字符串是不是指定的时间格式 -
     * @param str    时间字符串
     * @param format 要求的格式
     * @return 如何格式相同则返回 true，否则 false
     * 1. 比较格式和日期字符串的长度，因为即便设置了 sdf.setLenient(false); 那么 2023-12-12 12:12:12 依然能被 yyyy-MM-dd 识别
     * 2. 进行解析验证，不报错认为格式一致，否则认为格式不一致
     */
    public static boolean isValidDateFormat(String str, String format) {
        if (StringUtils.isEmpty(str) || StringUtils.isEmpty(format) || str.length() != format.length()) {
            return false;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setLenient(false);
            sdf.parse(str);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * 根据日期推导表达式
     *
     * @param value
     * @return
     */
    public static String guessPattern(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        String[] array = new String[]{"MM", "dd", "HH", "mm", "ss"};
        StringBuilder sourcePattern = new StringBuilder("yyyy");
        int arrayIndex = 0;
        int length = value.length() - 4;
        int tempIndex = 0;
        for (int i = 0; i < length; i++) {
            char c = value.charAt(i + 4);
            if (c >= '0' && c <= '9') {
                // tempIndex 为 0 或 2, 则追加表达式
                if ((tempIndex & 1) == 0) {
                    sourcePattern.append(array[arrayIndex++]);
                    tempIndex = 0;
                }
                tempIndex++;
            } else {
                sourcePattern.append(c);
                tempIndex = 0;
            }
        }
        return sourcePattern.toString();
    }

    /**
     * 按输入格式化日期并格式输出(必须以yyyy开头)
     * @param value   格式化字符串
     * @param pattern 输出格式
     */
    public static String convertFormat(String value, String pattern) {
        return convertFormat(value, pattern, Locale.getDefault());
    }

    /**
     * 按输入格式化日期并本地化格式输出(必须以yyyy开头)
     * @param value   格式化字符串
     * @param pattern 输出格式
     * @param locale  本地化
     */
    public static String convertFormat(String value, String pattern, Locale locale) {
        if (StringUtils.isEmpty(value)) {
            return StringUtils.EMPTY;
        }
        String sourcePattern = guessPattern(value);
        return convertFormat(value, sourcePattern, pattern, locale);
    }

    public static String convertFormat(String dateStr, String dateFormat, String targetFormat) {
        return convertFormat(dateStr, dateFormat, targetFormat, Locale.CHINA);
    }

    /**
     * 日期字符串格式转换
     *
     * @param dateStr      源日期字符串 	eg: 2023-12-12
     * @param dateFormat   源日期格式	eg: yyyy-MM-dd
     * @param targetFormat 要转换的日期格式 eg: yyyy年M月d日
     * @param targetLocale 语言模型，默认是中文格式，如果要转英文需要带上参数值: Locale.ENGLISH
     * @return
     */
    public static String convertFormat(String dateStr, String dateFormat, String targetFormat, Locale targetLocale) {
        if (StringUtils.isBlank(dateStr)) return StringUtils.EMPTY;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            Date date = sdf.parse(dateStr);
            return format(date, targetFormat, targetLocale);
        } catch (Exception e) {
            e.printStackTrace();
            return StringUtils.EMPTY;
        }
    }


    /**
     * =============================================时间计算部分===========================开始=========================
     */

    /**
     * 获取前n天的时间，返回当天的0点
     *
     * @param n
     */
    public static Date getPrvDay(int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -n);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取后n天的时间，返回当天的23点59分59秒
     *
     * @param n
     */
    public static Date getNextDay(int n) {
        return getNextDay(new Date(), n);
    }

    /**
     * 获取后n天的时间，返回当天的23点59分59秒
     *
     * @param n
     */
    public static Date getNextDay(Date date, int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +n);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    /**
     * 获取m月的前n月
     *
     * @param m （1-12月)
     * @param n
     */
    public static int getNextMonth(int m, int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, m - n - 1);
        return calendar.get(Calendar.MONTH);
    }

    /**
     * 获取本月的最后一天
     */
    public static Date getLastDayOfMonth() {
        return getLastDayOfMonth(new Date());
    }

    /**
     * 获取本月的最后一天
     *
     * @param now 当前时间
     */
    public static Date getLastDayOfMonth(Date now) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    /**
     * 获取month月的最后一天
     *
     * @param month 月份，从1开始
     */
    public static Date getLastDayOfMonth(int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    /**
     * 获取month月的第一天
     *
     * @param month 月份，从1开始；如果为null，则返回本月的第一天
     */
    public static Date getFirstDayOfMonth(Integer month) {
        if (month == null) {
            return getFirstDayOfMonth();
        } else {
            return getFirstDayOfMonth((int) month);
        }
    }

    /**
     * 获取本月的第一天
     *
     * @param now 当前时间
     */
    public static Date getFirstDayOfMonth(Date now) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取month月的第一天
     *
     * @param month 月份，从1开始
     */
    public static Date getFirstDayOfMonth(int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取本月的第一天
     */
    public static Date getFirstDayOfMonth() {
        return getFirstDayOfMonth(new Date());
    }



    public static void main(String[] args) throws ParseException {
        String s1 = "2023";
        String s11 = "2023.";
        String s2 = "202301";
        String s3 = "2023-1";
        String s4 = "2023.1";
        String s5 = "20230102";
        String s6 = "2023.01-02";
        String s7 = "2023.1.02";
        String s8 = "2023-1-02";
        String s9 = "2023/1/02";
        String s10 = "2023-1-02 3:04:5";
        String s12 = "2023年1月2号13点4分5秒";
        String s13 = "2023/1/02 13:04:5";
    }

}
