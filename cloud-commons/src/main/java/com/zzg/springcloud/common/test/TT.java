package com.zzg.springcloud.common.test;

import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.entity.Student;
import com.zzg.springcloud.common.result.Page;
import com.zzg.springcloud.common.result.ReturnResult;

public class TT {

    public static void main(String[] args) {

        Student student = new Student();
        student.setPassword("1qaz");
        student.setName("张志刚");
        student.setId(123L);

        ReturnResult rrt = ReturnResult.get();
        rrt.setData(student);

        System.out.println("rrt = " + rrt);

        Object obj = rrt.getData();
        System.out.println("obj = " + obj);
        System.out.println("JSONObject.toJSONString(obj) = " + JSONObject.toJSONString(obj));

        Student stu = rrt.getData();
        System.out.println("stu = " + stu);
        System.out.println("JSONObject.toJSONString(stu) = " + JSONObject.toJSONString(stu));

        Page page = rrt.getData();
        System.out.println("page = " + page);
        System.out.println("JSONObject.toJSONString(page) = " + JSONObject.toJSONString(page));

    }

}
