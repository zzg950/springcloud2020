package com.zzg.springcloud.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    /**
     * 定义一些long类型的时间毫秒值
     */
    public static final long MINUTE = 60 * 1000L;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;
    public static final long WEEK = DAY * 7;

    public static final String U_MINUTE = "MINUTE";
    public static final String U_HOUR = "HOUR";
    public static final String U_DAY = "DAY";
    public static final String U_WEEK = "WEEK";
    public static final String U_MONTH = "MONTH";
    public static final String U_YEAR = "YEAR";

    public static final String first_second = " 00:00:00";
    public static final String last_second = " 23:59:59";

    /**
     * 定义一些常用的 format 类型
     * 解释:
     * 年份 : yyyy （4位数年份2022）， yy （2位数年份22） 大小写没有区别
     * 月份 : MMMM （英文或者中文的月份） 英文全称
     *       MMM  （英文或者中文的月份） Locale.ENGLISH 时 值为 Mar ； 参数 Local.CHINA 时 值为 三月
     *       MM    （2位数的月份 03）， M （前面不带0的月份） 如果是3月则输出3，如果是12月则输出12
     * 星期 : EEEE - 英文的星期全称 Saturday，中文的星期三 ； EEE - 英文的星期简称 Sat，中文的星期三 ； EEE = EE = E
     * 日期 : dd   （2位的日期）， d（1位的日期）
     * 小时 : HH   （24小时制2位的小时） H （24小时制1位的小时） hh（12小时制2位小时） h（12小时制1位的小时）
     * 分钟 : mm   （2位的分钟）， m（1位的分钟）
     * 秒钟 : ss   （2位的秒钟）， s（1位的秒钟）
     * 午时 : a （标准使用a） 英文是： AM PM 中文是： 上午 下午
     * 毫秒 : SSS
     * 标准 : zzzz 标准全称； z 标准的简称； 英文是：China Standard Time  中文是：中国标准时间
     *
     * eg: String pattern = "年份（YYYY-YYY-YY-Y）; 月份（MMMM-MMM-MM-M）; 星期（EEEE-EEE-EE-E）; 日期（dddd-ddd-dd-d）; ";
     *         pattern += "小时（HH-hh-H-h）; 分钟（mmmm-mmm-mm-m）; 秒（ssss-sss-ss-s）; ";
     *         pattern += "（天数）DDDD-DDD-DD-D; 毫秒（SSSS-SSSS-SS-S）; 标准（zzzz-zzz-zz-z）";
     *     SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);
     *     System.out.println(simpleDateFormat.format(new Date()));
     *     simpleDateFormat = new SimpleDateFormat(pattern, Locale.CHINA);
     *     System.out.println(simpleDateFormat.format(new Date()));
     * 英文版：年份（2023-2023-23-2023）; 月份（March-Mar-03-3）; 星期（Saturday-Sat-Sat-Sat）; 日期（0025-025-25-25）;
     *       小时（13-01-13-01-13-1）; 分钟（0034-034-34-34）; 秒（0022-022-22-22）;
     *       （天数）0084-084-84-84; 毫秒（0585-0585-585-585）; 标准（China Standard Time-CST-CST-CST）
     * 中文版：年份（2023-2023-23-2023）; 月份（三月-三月-03-3）; 星期（星期六-星期六-星期六-星期六）; 日期（0025-025-25-25）;
     *       小时（13-01-13-01-13-1）; 分钟（0034-034-34-34）; 秒（0022-022-22-22）;
     *       （天数）0084-084-84-84; 毫秒（0598-0598-598-598）; 标准（中国标准时间-CST-CST-CST）
     */
    public static final String format_default_date = "yyyy-MM-dd";
    public static final String format_default_datetime = "yyyy-MM-dd HH:mm:ss";
    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final String yyyy = "yyyy";
    public static final String yyyyMM = "yyyyMM";
    public static final String yyyyMMdd = "yyyyMMdd";
    public static final String yyyyMMddHH = "yyyyMMddHH";
    public static final String yyyyMMddHHmm = "yyyyMMddHHmm";
    public static final String yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String yyyyMMddHHmmssSSS = "yyyyMMddHHmmssSSS";
    public static final String format_chinese_date = "YYYY年M月d日";
    public static final String format_english_date = "MMM. dd, yyyy";   // 英文中常用的日期格式
    public static final String format_english_full = "EEE MMM dd HH:mm:ss z yyyy";  // 英文版年月日，时分秒，时区和星期，需要配合 Locale.ENGLISH 使用
    public static final String ENGLISH_PRINT_FORMAT = "MMM. d, yyyy";
    public static final String CHINESE_PRINT_FORMAT = "yyyy年M月d日";

    private DateUtils() {}

    /**
     * =============================================时间格式部分===========================开始=========================
     * format 方法是最基本的方法，后面所有时间格式都是基于此方法进行处理
     * format_xxxx : xxxx 如果是date则表示默认的格式是 : yyyy-MM-dd; xxxx 如果是datetime则表示默认的格式是 yyyy-MM-dd HH:mm:ss
     */
    public static String format(Date date, String format, Locale locale) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
        return sdf.format(date);
    }
    public static String format(Date date, String format) {
        return format(date, format, Locale.CHINA);
    }

    public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_date);
        return sdf.format(new Date());
    }

    public static String getDatetime() {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_datetime);
        return sdf.format(new Date());
    }

    public static String getDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getDatetime(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getDate(long millisecond) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_date);
        return sdf.format(new Date(millisecond));
    }

    public static String getDatetime(long millisecond) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_datetime);
        return sdf.format(new Date(millisecond));
    }

    public static String getDate(long millisecond, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(millisecond));
    }

    public static String getDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_date);
        return sdf.format(date);
    }

    public static String getDatetime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(format_default_datetime);
        return sdf.format(date);
    }

    public static String getDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static Date getDateByStr(String datestr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.parse(datestr);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Date getDateByStr(String datestr) {
        return getDateByStr(datestr, format_default_datetime);
    }

    public static Date getDateByDatetimeStr(String datestr) {
        return getDateByStr(datestr, format_default_datetime);
    }

    public static Date getDateByDateStr(String datestr) {
        return getDateByStr(datestr, format_default_date);
    }

    /**
     * 在打印或者下载的文件中，所有的日期格式尽量保持统一
     * 中文的要求是 : 2023年3月25日
     * 英文的要求是 : Mar. 25, 2023
     */
    // 获取中文的打印日期: 默认当前日期，格式是 2023年3月25日
    public static String getChinesePrintDate() {
        return format(new Date(), format_chinese_date);
    }

    // 获取英文的打印日期: 默认当前日期， 格式是 Mar. 25, 2023
    public static String getEnglishPrintDate() {
        return format(new Date(), format_english_date, Locale.ENGLISH);
    }



}







