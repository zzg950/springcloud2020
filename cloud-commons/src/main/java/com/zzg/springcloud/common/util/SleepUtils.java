package com.zzg.springcloud.common.util;

import java.util.concurrent.TimeUnit;

/**
 * 线程睡眠工具类
 * 1. 增加一些方法实现随机睡眠时间
 * 2. 改在成运行时异常，缩短实际代码的数量
 */
public class SleepUtils {

    public static void sleepMilliseconds(int millisecond) {
        try {
            TimeUnit.MILLISECONDS.sleep(millisecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static void sleepMilliseconds(int minMillisecond, int maxMillisecond) {
        sleepMilliseconds(RandomUtils.getRandom(minMillisecond, maxMillisecond));
    }

    public static void sleepSeconds(int second) {
        sleepMilliseconds(second * 1000);
    }

    public static void sleepSeconds(int minSecond, int maxSecond) {
        sleepMilliseconds(RandomUtils.getRandom(minSecond, maxSecond) * 1000);
    }

    public static void sleepMinutes(int minute) {
        sleepMilliseconds(minute * 1000 * 60);
    }

    public static void sleepMinutes(int minMinute, int maxMinute) {
        sleepMilliseconds(RandomUtils.getRandom(minMinute, maxMinute) * 1000 * 60);
    }
}
