package com.zzg.springcloud.common.util;

import java.util.HashSet;
import java.util.Set;

/**
 * 当前类是完全通过 chatgpt 编写。 问题是 ： 用JAVA帮我写一个需求实现吧，在集群环境的部署下，要求生成一个全是数字的全局唯一字符串。用于替换常用的UUID
 *
 * 使用Snowflake算法生成全局唯一ID的实现，该算法生成的ID为64位整数，可以将其转换为全数字字符串。
 * 其中，`1`表示当前的worker ID，可根据实际情况进行配置。同时，使用Snowflake算法生成的全局唯一ID具有一定的顺序性，因此不适合需要保证随机性的场景。
 *
 * Snowflake算法中的worker ID，是用来标识生成ID的节点的，在集群环境下，不同的节点可以设置不同的worker ID，以确保生成的ID的唯一性。
 * worker ID的位数可以根据需求进行调整，一般情况下，10个二进制位可以表示1024个不同的节点。
 * 对于Snowflake算法而言，worker ID的作用在于防止同一时间、同一节点、同一序列号下生成的ID重复，也就是避免节点间序列冲突。
 * 在多节点场景下，如果worker ID不唯一，就会存在不同节点之间的ID冲突。
 * 因此，worker ID的选择应该尽可能地保证节点的唯一性，常用的设置方式是使用数据中心ID和节点ID进行组合，比如将10个二进制位分为5位节点ID和5位数据中心ID来表示。
 */
public class SnowFlakeUtils {

    private long workerId;
    private long sequence =0L;
    private long lastTimestamp = -1L;
    private final long dataCenterId =0L;                // 数据中心ID，默认为0
    private static final long TW_EPOCH =1288834974657L; // 起始时间戳，2010-11-0409:42:54.657
    private static final long WORKER_ID_BITS =5L;
    private static final long DATA_CENTER_ID_BITS =5L;
    private static final long MAX_WORKER_ID = ~(-1L << WORKER_ID_BITS);
    private static final long MAX_DATA_CENTER_ID = ~(-1L << DATA_CENTER_ID_BITS);
    private static final long SEQUENCE_BITS =12L;

    private static final long WORKER_ID_SHIFT = SEQUENCE_BITS;
    private static final long DATA_CENTER_ID_SHIFT = SEQUENCE_BITS + WORKER_ID_BITS;
    private static final long TIMESTAMP_LEFT_SHIFT = DATA_CENTER_ID_SHIFT + DATA_CENTER_ID_BITS;
    private static final long SEQUENCE_MASK = ~(-1L << SEQUENCE_BITS);

    public SnowFlakeUtils(long workerId) {
        if (workerId > MAX_WORKER_ID || workerId <0) {
            throw new IllegalArgumentException(String.format("workerId can't be greater than %d or less than0", MAX_WORKER_ID));
        }
        this.workerId = workerId;
    }

    public synchronized long nextId() {
        long timestamp = timeGen();

        if (timestamp < lastTimestamp) {
            throw new RuntimeException(String.format("Clock moved backwards. Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }

        if (lastTimestamp == timestamp) {
            sequence = (sequence +1) & SEQUENCE_MASK;
            if (sequence ==0) {
                timestamp = tilNextMillis();
            }
        } else {
            sequence =0L;
        }

        lastTimestamp = timestamp;

        long id = ((timestamp - TW_EPOCH) << TIMESTAMP_LEFT_SHIFT) |
                (dataCenterId << DATA_CENTER_ID_SHIFT) |
                (workerId << WORKER_ID_SHIFT) |
                sequence;

        return id;
    }

    private long tilNextMillis() {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    private long timeGen() {
        return System.currentTimeMillis();
    }

    public static String generateUniqueId(long workerId) {
        SnowFlakeUtils generator = new SnowFlakeUtils(workerId);
        long uniqueId = generator.nextId();
        String uniqueIdStr = String.valueOf(uniqueId);
        return uniqueIdStr;
    }

    /**
     * 在当前实现中，由于 timeGen 使用的是 毫秒，所有在同一个毫秒内生成的数据，并且workId相同的情况下会发生重复
     * 如果将 timeGen 方法中的 System.currentTimeMillis(); 改成 System.nanoTime(); 取其纳秒值，则不会重复了
     *
     * 同时，为了提高时间戳的精度，使用了System.nanoTime()获取当前纳秒级别的时间戳，而不是System.currentTimeMillis()获取毫秒级别的时间戳。
     *
     * 需要注意的是，使用System.nanoTime()生成时间戳的方式存在一定的限制，可能会受到系统时间调整、系统时间回拨等因素影响，因此不适合在需要保证时间戳绝对唯一的场景中使用。
     *
     * 另外需要牢记 workId 的用途，在集群部署时，需要通过外部配置，来保证不同机器上 workId 的不同
     */
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            set.add(SnowFlakeUtils.generateUniqueId(2));
        }
        System.out.println("set.size() = " + set.size());
        System.out.println(MAX_WORKER_ID);
        Set<Long> setH = new HashSet<>();
        Set<Long> setN = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            setH.add(System.currentTimeMillis());
            setN.add(System.nanoTime());
        }
        System.out.println("setH.size() = " + setH.size());
        System.out.println("setN.size() = " + setN.size());
    }
}
