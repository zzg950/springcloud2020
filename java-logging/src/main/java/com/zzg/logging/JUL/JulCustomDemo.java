package com.zzg.logging.JUL;


import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * jul = java.util.logging
 * 1. 使用自定义的配置文件
 * 2. 加载配置文件
 * 3. 指定等级
 * 4. 配置 handler（appender）
 * 5. 不同的包使用不同的等级
 */
public class JulCustomDemo {

    static LogManager logManager = LogManager.getLogManager();

    static {
        try {
            /**
             * 1. 在配置文件中修改日志等级为 ALL 进行测试  --  默认是 INFO
             * 2. 在配置文件中修改不同包的日志等级 -- 了解到配置方法跟定义 logger 时的参数是有关系的
             */
            logManager.readConfiguration(JulCustomDemo.class.getClassLoader().getResourceAsStream("logging.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final Logger logger = Logger.getLogger(JulCustomDemo.class.getName());

    public static void main(String[] args) {
        logger.finest("This is Finest --> 最好的");
        logger.finer("This is Finer --> 较好的");
        logger.fine("This is Finest --> 好的");
        logger.config("This is Config --> 配置的");
        logger.info("This is Info --> 消息的");
        logger.warning("This is Warning --> 警告的");
        logger.severe("This is Severe --> 糟糕的");
    }


}
