package com.zzg.logging.JUL;


import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * jul = java.util.logging
 * JDK 自带的 日志工具 不需要引入额外的 jar包
 * 1. 了解 logger 的等级制度
 * 2. 了解 logger 的父子关系
 * 3. 手动修改 logger 的输出级别
 * <p>
 * jul 也有自己的配置文件 - 默认使用jdk自带的配置文件 $JAVA_HOME/jre8/lib/logging.properties
 */
public class JulDefaultDemo {

    private static final Logger logger = Logger.getLogger("abc");

    public static void main(String[] args) {

        System.out.println("logger.getParent().getName() = " + logger);
        System.out.println("logger.getName() = " + logger.getName());
        System.out.println("logger.getLevel() = " + logger.getLevel());

        // 设置本身logger
        logger.setLevel(Level.ALL);

        Handler[] handlers = logger.getParent().getHandlers();
        for (Handler handler : handlers) {
            System.out.println(handler.getLevel());
            handler.setLevel(Level.ALL);
        }

        // 1. 展示各种日志级别  ALL<FINEST<FINER<FINE<CONFIG<INFO<WARNING<SEVERE<OFF
        // 2. 每一种都对应一种简写的方式
        // 3. 在控制台输出是暗红色的 - 因为使用 System.err 输出流输出的
        // 4. 默认值只输出 INFO 以及后面的级别
        logger.log(Level.FINEST, "This is Finest --> 最好的");
        logger.log(Level.FINER, "This is Finer --> 较好的");
        logger.log(Level.FINE, "This is Finest --> 好的");
        logger.log(Level.CONFIG, "This is Config --> 配置的");

        logger.log(Level.INFO, "This is Info --> 消息的");
        logger.log(Level.WARNING, "This is Warning --> 警告的");
        logger.log(Level.SEVERE, "This is Severe --> 糟糕的");

        logger.info("This is info 2.0 ---> 简写版 ---> 消息");
        logger.severe("This is severe 2.0 ---> 简写版 ---> 糟糕的");

    }

}
