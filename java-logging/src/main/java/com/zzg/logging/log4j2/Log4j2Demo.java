package com.zzg.logging.log4j2;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.UUID;

/**
 * 1. 默认在classpath中寻找配置文件的优先级如下:
 * log4j2-test.json、 log4j2-test.jsn、 log4j2-test.xml、 log4j2.json、 log4j2.jsn、 log4j2.xml
 * 2. 一般默认使用 log4j2.xml 进行配置
 * 3. 如果找不到对应的配置文件，则启用默认的配置：输出 error 以上的错误，且只输出到控制台
 */
public class Log4j2Demo {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) throws Exception {
        String traceId = UUID.randomUUID().toString();
        for (; ; ) {
            logger.trace("This is INFO; 消息内容 = {}", "INFO");
            logger.debug("This is DEBUG; 消息内容 = {}", "DEBUG");
            logger.info("This is INFO; 消息内容 = {}, {}", () -> traceId, () -> getContent());
            logger.warn("This is WARN; 消息内容 = {}", "WARN");
            logger.error("This is ERROR; 消息内容 = {}", "ERROR");
            logger.error("=====================================================");
            Thread.sleep(3000);
        }
    }

    public static String getContent() {
        System.out.println("~~~~~~~~~~~~~~~~~~~");
        return "cool --- 酷酷酷";
    }

}
