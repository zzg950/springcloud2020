package com.zzg.mybatis;


import com.zzg.springcloud.common.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisApplicationStart {

    private static final Logger log = LoggerFactory.getLogger(MybatisApplicationStart.class);

    public static void main(String[] args) {
        SpringApplication.run(MybatisApplicationStart.class, args);
        log.info("======MybatisApplicationStart    启动成功======");
        System.out.println(DateUtils.getDatetime() + "\t======MybatisApplicationStart    启动成功======");
    }

}
