package com.zzg.springcloud.order.feignclient;

import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.result.ReturnResult;
import feign.Request;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(value = "CLOUD-PAYMENT", path = "payment")
public interface PaymentClient {

    /**
     * 被调用的服务的controller里面方法怎么写，这里就怎么写，必须一致，
     * 比如: 传参必须使用 @PathVariable 修饰, 必须使用 @RequestBody 修饰等 - 例如: /post/{id}
     * 原接口直接接受的参数，需要在这里使用 @RequestParam 修饰，否则 405 错误
     */
    @GetMapping(value = "/get/{id}")
    ReturnResult get(@PathVariable("id") String id);

    @PostMapping(value = "/post/{id}")
    ReturnResult post(@PathVariable("id") String id, @RequestBody JSONObject params);

    @GetMapping(value = "/discovery")
    ReturnResult discovery();

    // 通过 Request.Options 对象为不同的接口设置不同的超时时间，在发起请求的时候配置
    @GetMapping(value = "/sleep/{million}")
    ReturnResult sleep(Request.Options options, @PathVariable("million") Long million);

    @GetMapping(value = "/sleep2")
    ReturnResult sleep2(@RequestParam("million") Long million);
}
