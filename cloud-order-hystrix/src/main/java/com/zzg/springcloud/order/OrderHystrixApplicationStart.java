package com.zzg.springcloud.order;

import com.zzg.springcloud.common.util.DateUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class OrderHystrixApplicationStart {

    public static void main(String[] args) {
        SpringApplication.run(OrderHystrixApplicationStart.class, args);
        System.out.println(DateUtils.getDatetime() + "\t======OrderHystrixApplicationStart    启动成功======");
    }

}
