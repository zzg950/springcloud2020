package com.zzg.springcloud.order.test;

import feign.Request;

public class T {
    public static void main(String[] args) {
        Request.Options options = new Request.Options();
        System.out.println("connectTimeoutMillis --> " + options.connectTimeoutMillis());
        System.out.println("readTimeoutMillis --> " + options.readTimeoutMillis());
        System.out.println("isFollowRedirects --> " + options.isFollowRedirects());

        T t = new T();
        System.out.println(t);
        System.out.println("t.hashCode() = " + t.hashCode());
        System.out.println("Integer.toBinaryString(t.hashCode()) = " + Integer.toBinaryString(t.hashCode()));
        System.out.println("Integer.toHexString(t.hashCode()) = " + Integer.toHexString(t.hashCode()));
        System.out.println("Integer.toOctalString(t.hashCode()) = " + Integer.toOctalString(t.hashCode()));

        System.out.println("Float.floatToIntBits(123.455f) = " + Float.floatToIntBits(123.455f));
    }
}
