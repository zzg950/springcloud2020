package com.zzg.thread.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 测试几个Map接口的实现类
 * 证明哪些是线程安全的类，哪些是线程不安全的类
 * 1. 线程不安全的体现在哪里？
 * 参考 ListDemo答案
 * 2. java.util.ConcurrentModificationException 出现的原因是什么？
 * 参考 ListDemo答案
 */
public class MapDemo {

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();                  // HashMap 的 value 可以为空
//        Map<String, Object> map = new TreeMap<>();
//        Map<String, Object> map = new LinkedHashMap<>();
//        Map<String, Object> map = Collections.synchronizedMap(new HashMap<>());
//        Map<String, Object> map = new ConcurrentHashMap<>();      // map.put("123", null); 报错， ConcurrentHashMap 的 value 不能为空

        for (int i = 1; i <= 30; i++) {
            final int tmpint = i;
            new Thread(() -> {
                map.put(UUID.randomUUID().toString().substring(0, 8) + "~" + tmpint, "VALUE");
                System.out.println(map.size());
            }, "Thread-Name-" + i).start();
        }

        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println("=============================");
        System.out.println(map);
        System.out.println(map.size());
    }
}
