package com.zzg.thread.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * 测试几个Set接口的实现类
 * 证明哪些是线程安全的类，哪些是线程不安全的类
 * 1. 线程不安全的体现在哪里？
 * 参考 ListDemo答案
 * 2. java.util.ConcurrentModificationException 出现的原因是什么？
 * 参考 ListDemo答案
 */
public class SetDemo {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
//        Set<String> set = new TreeSet<>();
//        Set<String> set = new LinkedHashSet<>();
//        Set<String> set = Collections.synchronizedSet(new HashSet<>());
//        Set<String> set = new CopyOnWriteArraySet<>();

        for (int i = 1; i <= 30; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(set);
            }, "Thread-Name-" + i).start();
        }

        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println("=============================");
        System.out.println(set);
        System.out.println(set.size());
    }
}
