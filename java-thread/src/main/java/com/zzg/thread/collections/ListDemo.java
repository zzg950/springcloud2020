package com.zzg.thread.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 测试几个List接口的实现类
 * 证明哪些是线程安全的类，哪些是线程不安全的类
 * 1. 线程不安全的体现在哪里？
 * 答: 30个线程并发往一个线程不安全的List里面add数据，执行的结果有可能只有29个28个，少于30个；即便有30个结果里面可能有一些为null的值；
 * 说明有一些数据没有存进去，数据有丢失。
 * 2. java.util.ConcurrentModificationException 出现的原因是什么？
 * 答: 导致这个错误的原因是在多线程中，有的线程对List进行update操作，有的线程对List进行读取操作，
 * 当正在读取的线程，在读取数据的过程中，List的长度发生改变，则报此异常。
 */
public class ListDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
//        List<String> list = new LinkedList<>();
//        List<String> list = new Vector<>();
//        List<String> list = Collections.synchronizedList(new ArrayList<>());
//        List<String> list = new CopyOnWriteArrayList<>();

        for (int i = 1; i <= 30; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(list);
            }, "Thread-Name-" + i).start();
        }

        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println("=============================");
        System.out.println(list);
        System.out.println(list.size());
    }
}
