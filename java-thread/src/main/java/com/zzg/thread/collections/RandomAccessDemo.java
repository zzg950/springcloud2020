package com.zzg.thread.collections;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * 判断 ArrayList 和 LinkedList 的性能和区别
 * <p>
 * ArrayList 实现了 RandomAccess 接口和 List 接口
 * LinkedList 实现了 List 和 Deque 接口，没有实现 RandomAccess 接口，LinkedList 的底层其实是一个队列
 * <p>
 * 结论： ArrayList 使用 for 循环快
 * LinkedList 使用 迭代器(Iterator)遍历更快
 */

public class RandomAccessDemo {

    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        IntStream.range(1, 1000000).forEach(i -> arrayList.add(i));

        List<Integer> linkedList = new LinkedList<>(arrayList);

        System.out.println("============数据已经准备好了============");
        System.out.println("ArrayList使用For循环耗时: " + forList(arrayList));
        System.out.println("ArrayList使用ForEach循环耗时: " + forEachList(arrayList));
        System.out.println("ArrayList使用Iterator循环耗时: " + iteratorList(arrayList));
        System.out.println("LinkedList使用For循环耗时: " + forList(linkedList));
        System.out.println("LinkedList使用ForEach循环耗时: " + forEachList(linkedList));
        System.out.println("LinkedList使用Iterator循环耗时: " + iteratorList(linkedList));

    }

    public static long forList(List<Integer> list) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++) {
            Integer integer = list.get(i);
        }
        return System.currentTimeMillis() - start;
    }

    public static long forEachList(List<Integer> list) {
        long start = System.currentTimeMillis();
        for (Integer integer : list) {
            int t = integer;
        }
        return System.currentTimeMillis() - start;
    }

    public static long iteratorList(List<Integer> list) {
        long start = System.currentTimeMillis();

        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            int i = it.next();
        }
        return System.currentTimeMillis() - start;
    }

}
