package com.zzg.thread.concurrent;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * volatile可以保证可见性，但是无法保证原子性，那么在多线程并发操作volatile修饰的数据的时候，
 * 仍然会出现脏数据，想要解决这个问题，可以使用AtomicInteger对象，可以作为计数器。
 * <p>
 * AtomicInteger 多线程计数器 - 在多线程操作下依然是线程安全的
 */
public class AtomicIntegerDemo {

    private static volatile int mm = 0;
    private static AtomicInteger atomicInteger = new AtomicInteger();

    public static void main(String[] args) {
        atomicIntegerTest();
    }

    /**
     * 当前示例可以证明，在多线程下修改同一个数据
     * volatile 修饰的变量无法保证原子性
     * AtomicInteger 对象可以保证操作的原子性
     */
    public static void atomicIntegerTest() {
        System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->begin-->mm=" + mm + "-->atomicInteger=" + atomicInteger);
        System.out.println("============并发修改数据开始=================");
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->begin-->mm=" + mm + "-->atomicInteger=" + atomicInteger);
                for (int j = 0; j < 1000; j++) {
                    mm++;
                    atomicInteger.getAndIncrement();
                }
                System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->end-->mm=" + mm + "-->atomicInteger=" + atomicInteger);
            }, "Thread-Name-" + i).start();
        }

        /**
         * 默认运行的有 main 和 GC 线程， 除此之外说明还有其他线程正在运行
         * 当主线程抢到CPU执行权限后判断当前活跃的线程数量如果超过2个, 则说明还有其他线程在执行, 从而让出CPU执行权, 从执行线程变为可执行线程
         * 由CPU再次选择 - 不过有可能主线程再次抢到
         * Thread.yield() : 使当前线程由执行状态，变成为就绪状态，让出cpu时间，在下一个线程执行时候，此线程有可能被执行，也有可能没有被执行。
         *
         */
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println("==============最终结果================");
        System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->volatileTest2 end-->mm=" + mm + "-->atomicInteger=" + atomicInteger);
    }

}
