package com.zzg.thread.concurrent;

import com.zzg.springcloud.common.util.RandomUtils;

import java.util.concurrent.*;

public class SemaphoreDemo2 {

    protected int core = 2;
    protected ThreadPoolExecutor executor;
    protected Semaphore semaphore;

    public static void main(String[] args) {
        SemaphoreDemo2 t = new SemaphoreDemo2();
        t.init();
        for (int i = 0; i < 10; i++) {
            System.out.println("~~~~~~~~~~" + i);
            t.execute();
        }
    }

    public void execute() {
        executor.execute(() -> {
            try {
                semaphore.acquire();
                System.out.println(System.currentTimeMillis() + "===" + Thread.currentThread().getName() + "~~~抢到线程执行~~~");
                int rand = RandomUtils.getRandom(2, 5);
                TimeUnit.SECONDS.sleep(rand);
                System.out.println(System.currentTimeMillis() + "===" + Thread.currentThread().getName() + "~~~执行" + rand + "秒后，离开~~");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                semaphore.release();
            }
        });
    }

    public void init() throws RuntimeException {
        semaphore = new Semaphore(core * 2);
        executor = new ThreadPoolExecutor(core,
                core * 2,
                0,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(core * 2),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.CallerRunsPolicy());
    }


}
