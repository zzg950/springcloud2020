package com.zzg.thread.threadpool;

import com.zzg.springcloud.common.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author 张志刚
 * @Date 2022/1/4
 * @Description TODO
 * JDK1.8中新增的CompletableFuture中通过函数式的编程方法提供了等同于异步回调的能力
 * <p>
 * 没有理解这个异步回调函数的应用场景
 * 既然是执行完才走异步回调，那么直接在线程里面执行完直接执行就可以了啊。
 * 是不是可以在不使用线程池的情况下单独使用异步，那应该并不比自己创建线程的性能高。
 */
public class CompletableFutureDemo {

    private static ExecutorService threadPool = Executors.newFixedThreadPool(5);

    public static void main(String[] args) {
        List<CompletableFuture<String>> futureList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            final int tmpInt = i;
            CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
                String str = "supplyAsync--> Thread-Name-" + tmpInt + "; random = " + RandomUtils.getRandom(100);
                System.out.println(str);
                return str;
            }, threadPool).thenApply(e -> {
                System.out.println("thenApply --> " + e);
                return "thenApply --> " + e;
            });
            futureList.add(future);
        }
        System.out.println("==========直接输出，标识异步操作======");
    }
}
