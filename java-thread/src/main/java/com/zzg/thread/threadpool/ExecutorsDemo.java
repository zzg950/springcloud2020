package com.zzg.thread.threadpool;


import com.zzg.springcloud.common.util.RandomUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * java.util.concurrent.Executors
 * 提供了一些列静态方法，用于创建一些常用的线程池
 * 只是对 new ThreadPoolExecutor() 方法的一些固定封装，没有什么特别之处
 * 但是由于这些静态方法提供的线程池不够灵活，所以《阿里巴巴java开发手册》中不建议使用
 * <p>
 * newSingleThreadExecutor - 创建单个线程的线程池
 * newFixedThreadPool      - 固定线程数量的线程池
 * newCachedThreadPool     - 无固定大小线程数量的线程池
 * 自定义线程池 以及 对应的 submit 方法和 execute 的方法区别，参考 ExecutorThreadPoolDemo 类
 */
public class ExecutorsDemo {

    /**
     * 固定数量线程池 - 数量只有一个，无界任务队列
     * 线程池内只有一个可用线程，可以保证提交给线程的任务可以按照顺序执行
     * 在需要异步任务，但是又需要按照提交任务的顺序进行执行的时候比较有用
     */
    private static ExecutorService singlePool = Executors.newSingleThreadExecutor();

    /**
     * 固定数量线程池 - 数量由参数固定 - 核心线程数 = 最大线程数，无界任务队列
     */
    private static ExecutorService fixedPool = Executors.newFixedThreadPool(3);

    /**
     * 无固定数量可缓冲线程池 - 启动时没有可用线程，线程池内线程可以开启的总数量范围是 0 ~ Integer.MAX_VALUE
     * 根据任务需要自动开启线程数量 - 不推荐使用，因为硬件可支持的线程数量是有限的
     */
    private static ExecutorService cachePool = Executors.newCachedThreadPool();


    public static void main(String[] args) {
//        newSingleThreadExecutorTest();
//        newFixedThreadPoolTest();
        newCachedThreadPoolTest();
    }


    /**
     * 使用 newSingleThreadExecutor 验证任务执行顺序是按照提交顺序处理
     */
    public static void newSingleThreadExecutorTest() {
        for (int i = 0; i < 10; i++) {
            final int tempInt = i;
            singlePool.execute(() -> {
                try {
                    int rand = RandomUtils.getRandom(2000);
                    TimeUnit.MILLISECONDS.sleep(rand);
                    System.out.println(String.format("threadName = %s; index = %d; sleep = %d", Thread.currentThread(), tempInt, rand));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    /**
     * 使用 newFixedThreadPool 验证固定线程数量的线程池
     */
    public static void newFixedThreadPoolTest() {
        for (int i = 0; i < 10; i++) {
            final int tempInt = i;
            fixedPool.execute(() -> {
                try {
                    int rand = RandomUtils.getRandom(200);
                    TimeUnit.MILLISECONDS.sleep(rand);
                    System.out.println(String.format("threadName = %s; index = %d; sleep = %d", Thread.currentThread(), tempInt, rand));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }


    /**
     * 使用 newCachedThreadPool 验证无固定数量可缓冲线程池
     */
    public static void newCachedThreadPoolTest() {
        for (int i = 0; i < 100; i++) {
            final int tempInt = i;
            cachePool.execute(() -> {
                try {
                    int rand = RandomUtils.getRandom(1000);
                    TimeUnit.MILLISECONDS.sleep(rand);
                    System.out.println(String.format("threadName = %s; index = %d; sleep = %d", Thread.currentThread(), tempInt, rand));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
