package com.zzg.thread.threadpool;

import lombok.SneakyThrows;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 自定义拒绝策略
 */
public class MyRejectedPolicy implements RejectedExecutionHandler {
    @Override
    @SneakyThrows
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
//        System.out.println(new Thread(r).getName() + "被抛弃~~" + executor.getQueue().getClass().getName().length());
        System.out.println(new Thread(r).getName() + "被抛弃~~" + executor.getQueue().size());
        executor.getQueue().poll();
        executor.getQueue().put(r);
    }
}
