package com.zzg.thread.threadpool;


import com.alibaba.fastjson.JSONObject;
import com.zzg.springcloud.common.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Future - 线程执行时返回的数据
 * 未完成的示例代码：
 * https://blog.csdn.net/hayre/article/details/53314599
 * https://www.cnblogs.com/dafanjoy/p/14505058.html
 */
public class ThreadPoolFutureDemo {


    private static ExecutorService taskPool = new ThreadPoolExecutor(5, 15, 1000,
            TimeUnit.MILLISECONDS,
            new ArrayBlockingQueue<>(10), new ThreadPoolExecutor.CallerRunsPolicy());


    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        singleThreadFutureTest();
        singleThreadFutureTest_2();
        multiThreadFutureTest();
    }

    /**
     * 单一线程获取返回值的写法
     */
    public static void singleThreadFutureTest() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        Future<String> future = taskPool.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                int rand = RandomUtils.getRandom(3000);
                TimeUnit.MILLISECONDS.sleep(rand);
                System.out.println("======线程内部打印执行代码===== rand = " + rand);
                return String.valueOf(rand);
            }
        });
        System.out.println("线程池执行工作线程");
        String res = future.get();  //注意这里get操作是阻塞，future仍属于同步返回，主线程需要阻塞等待结果返回
//        String res = future.get(3, TimeUnit.SECONDS);  // 为防止线程无限期等待，设置等待的超时时间
        System.out.println(Thread.currentThread() + "~~~ res = " + res);
        long end = System.currentTimeMillis();
        System.out.println("totalCost = " + (end - start));
    }

    /**
     * 单一线程获取返回值的写法
     * 使用 lambda 写法实现线程
     */
    public static void singleThreadFutureTest_2() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        Future<String> future = taskPool.submit(() -> {
            int rand = RandomUtils.getRandom(3000);
            TimeUnit.MILLISECONDS.sleep(rand);
            System.out.println("======线程内部打印执行代码===== rand = " + rand);
            return String.valueOf(rand);
        });
        System.out.println("线程池执行工作线程");
        String res = future.get();  //注意这里get操作是阻塞，future仍属于同步返回，主线程需要阻塞等待结果返回
//        String res = future.get(3, TimeUnit.SECONDS);  // 为防止线程无限期等待，设置等待的超时时间
        System.out.println(Thread.currentThread() + "~~~ res = " + res);
        long end = System.currentTimeMillis();
        System.out.println("totalCost = " + (end - start));
    }

    /**
     * 多线程线程获取返回值的写法
     * 使用 lambda 写法实现线程
     */
    public static void multiThreadFutureTest() {
        List<Future<JSONObject>> futureList = new ArrayList<>();

        AtomicInteger atomicCount = new AtomicInteger();

        for (int i = 0; i < 10; i++) {
            Future<JSONObject> future = taskPool.submit(() -> {
                JSONObject json = new JSONObject();
                int rand = RandomUtils.getRandom(3000);
                atomicCount.addAndGet(rand);
                return json;
            });
            futureList.add(future);
        }
    }

}
