package com.zzg.thread.threadpool;

import com.zzg.springcloud.common.util.RandomUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 配合 MethodProvideDemo 验证多线程下各种方法的安全性
 */
public class MethodCheckDemo2 {

    private ExecutorService fixedPool = Executors.newFixedThreadPool(50);

    private MethodProvideDemo staticProvideDemo = new MethodProvideDemo();

    public static void main(String[] args) {
        MethodCheckDemo2 demo2 = new MethodCheckDemo2();
        demo2.checkStaticMethod();
    }

    /**
     * 验证静态方法
     */
    public void checkStaticMethod() {
        MethodProvideDemo methodProvideDemo = new MethodProvideDemo();
        for (int i = 0; i < 10000; i++) {
            final int tempInt = i;
            fixedPool.execute(() -> {
                MethodProvideDemo innerProvideDemo = new MethodProvideDemo();
                int a = RandomUtils.getRandom(100);
                int b = RandomUtils.getRandom(100);

                int res1 = staticProvideDemo.add(a, b);
                int res2 = staticProvideDemo.syncAdd(a, b);

                int res3 = methodProvideDemo.add(a, b);
                int res4 = methodProvideDemo.syncAdd(a, b);

                int res5 = innerProvideDemo.add(a, b);
                int res6 = innerProvideDemo.syncAdd(a, b);

                int res7 = MethodProvideDemo.staticAdd(a, b);
                int res8 = MethodProvideDemo.syncStaticAdd(a, b);

                if (res1 == res2 && res2 == res3 && res3 == res4 && res4 == res5 && res5 == res6 && res6 == res7 && res7 == res8) {
                    if (tempInt % 100 == 0) {
                        System.out.println(String.format("index = %4d;", tempInt));
                    }
                } else {
                    System.out.println(String.format("static add : index = %4d; a = %d; b = %d; " +
                                    "res1 = %d; res2 = %d; res3 = %d; res4 = %d; res5 = %d; res6 = %d; res7 = %d; res8 = %d; ",
                            tempInt, a, b, res1, res2, res3, res4, res5, res6, res7, res8));
                }
            });
        }
    }
}
