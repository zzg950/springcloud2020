package com.zzg.thread.threadpool;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 方法提供者类 - 提供几种类型的方法，验证在多线程是否安全
 * 1. 提供一个静态方法，验证多线程下静态方法是否安全
 * 2. 提供一个非静态方法，验证多线程下是否安全
 * 3. 提供一个加锁的非静态方法，验证多线程下是否安全
 * <p>
 * 检测结果 - 无论是在静态方法还是非静态方法下，下面4种add方法均得到正确的结果
 * 参考 : MethodCheckDemo 和 MethodCheckDemo2 两个类
 * <p>
 * 那怎么能判断在多线程下 SimpleDateFormat 下的方法是不安全的呢？
 */
public class MethodProvideDemo {

    public int add(int a, int b) {
        return a + b;
    }

    public static int staticAdd(int a, int b) {
        return a + b;
    }

    public synchronized int syncAdd(int a, int b) {
        return a + b;
    }

    public static synchronized int syncStaticAdd(int a, int b) {
        return a + b;
    }

    private final static SimpleDateFormat staticSdf = new SimpleDateFormat("yyyyMMddHHmmss");

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    public String format(long millisecond) {
        return this.sdf.format(new Date(millisecond));
    }

    /**
     * 1. 不安全
     * 2. 调用类
     */
    public synchronized String syncFormat(long millisecond) {
        return this.sdf.format(new Date(millisecond));
    }

    public static String staticFormat(long millisecond) {
        return staticSdf.format(new Date(millisecond));
    }

    public static synchronized String syncStaticFormat(long millisecond) {
        return staticSdf.format(new Date(millisecond));
    }

    public static void main(String[] args) {
        String res = MethodProvideDemo.staticFormat(1651556883224L);
        System.out.println(res);
    }
}
