package com.zzg.thread.threadlocal;


import java.util.UUID;

/**
 * 使用 ThreadLocal 解决线程内数据传递的问题
 * 提供一个中间方法，所有用到的地方都来这里获取
 *
 * @author 张志刚
 * <p>
 * 参考 : https://blog.csdn.net/weixin_43864927/article/details/116020602
 * <p>
 * 线程池-->ThreadLocal数据传递
 * 参考 : https://blog.csdn.net/weixin_43864927/article/details/116209746
 */
public class ThreadLocalUtils {

    /**
     * 示例解释:
     * 如果使用 ThreadLocal 实现线程内的数据传递，那么线程创建和调用的子线程会新建ThreadLocal对象，数据不会共享
     * 执行调用 ThreadLocalDemo4 中的 main 方法时，main线程、AA线程、BB线程中的 traceId 不一样
     * <p>
     * 如果使用 InheritableThreadLocal 实现线程内的数据传递，那么数据将会被传递到自己创建和调用的子线程上
     * 执行调用 ThreadLocalDemo4 中的 main 方法时，main线程、AA线程、BB线程中的 traceId 完全一样
     */
//    private static final ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();
    private static final ThreadLocal<String> THREAD_LOCAL = new InheritableThreadLocal<>();
    /**
     * 可以在线程初始化的时候直接初始化数据
     */
    private static final ThreadLocal<String> THREAD_LOCAL_NEW = new ThreadLocal<String>() {
        @Override
        public String initialValue() {
            return UUID.randomUUID().toString().replace("-", "");
        }
    };
    // 使用 lamada 表达式初始化数据
    private static final ThreadLocal<String> THREAD_LOCAL_NEW2 = ThreadLocal.withInitial(() -> UUID.randomUUID().toString().replace("-", ""));

    /**
     * 初始化线程中保存的值
     *
     * @return String
     */
    public static String init() {
        String traceId = UUID.randomUUID().toString().replace("-", "");
        THREAD_LOCAL.set(traceId);
        return traceId;
    }

    /**
     * 获取线程中保存的值
     *
     * @return String
     */
    public static String get() {
        return THREAD_LOCAL.get() == null ? init() : THREAD_LOCAL.get();
    }

    /**
     * 如果线程中的值为空，则将当前值赋值，然后返回
     *
     * @return String
     */
    public static String get(String traceId) {
        return THREAD_LOCAL.get() == null ? set(traceId) : THREAD_LOCAL.get();
    }

    /**
     * 设置线程需要保存的值
     *
     * @return String
     */
    public static String set(String traceId) {
        THREAD_LOCAL.set(traceId);
        return traceId;
    }

    /**
     * 移除线程中保存的值
     */
    public static void remove() {
        THREAD_LOCAL.remove();
    }
}
