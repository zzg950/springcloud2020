package com.zzg.thread.threadlocal;


import java.util.UUID;

/**
 * 1. ThreadLocal 解决线程内部多个方法之间传递公共参数的问题，解决线程内部数据共享问题；引发问题是不能往子线程传递数据。
 * 2. InheritableThreadLocal 解决父子线程之间数据传递问题；引发的问题是无法在使用线程池时正常传递数据到执行任务的线程；
 * 3. TransmittableThreadLocal 解决线程池情况下数据传递问题。
 * <p>
 * Inheritable --> 可继承的
 * InheritableThreadLocal --> 可继承的ThreadLocal --> 用于父子线程之间共用ThreadLocal
 * <p>
 * InheritableThreadLocal类原理简介使用 父子线程传递数据详解 多线程中篇（十八）
 * https://cloud.tencent.com/developer/article/1403608
 * <p>
 * 全链路跟踪(压测)必备基础组件之线程上下文“三剑客”
 * https://cloud.tencent.com/developer/article/1480608
 */
public class InheritableThreadLocalDemo {

    // 只能单个线程使用 - 单个线程设置自己的数据，可获取自己设置的数据，新建的子线程无法获取数据
//    private static final ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();

    // 可实现线程之间的传递，在新建子线程时，会复制父线程的ThreadLocal数据到子线程，产生两份数据，之后互不影响
    private static final ThreadLocal<String> THREAD_LOCAL = new InheritableThreadLocal<>();

    public static void main(String[] args) throws InterruptedException {

        THREAD_LOCAL.set(UUID.randomUUID().toString() + "-before change");

        String format = "threadName = %s; traceId = %s;";
        System.out.println(String.format(format, "main", THREAD_LOCAL.get()));

        new Thread(() -> {
            for (; ; ) {
                try {
                    System.out.println(String.format(format, Thread.currentThread().getName(), THREAD_LOCAL.get()));
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1").start();

        Thread.sleep(5000);

        THREAD_LOCAL.set(UUID.randomUUID().toString() + "-after change");
        for (; ; ) {
            System.out.println(String.format(format, "main", THREAD_LOCAL.get()));
            Thread.sleep(1000);
        }

    }

}
