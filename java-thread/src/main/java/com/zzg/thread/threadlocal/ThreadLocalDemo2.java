package com.zzg.thread.threadlocal;

/**
 * 相对来说，使用中间类 ThreadLocalUtils 是最方便的
 * 证明: 使用 ThreadLocalUtils 在不同的类之间传递数据
 */
public class ThreadLocalDemo2 {

    public static void main(String[] args) {
        ThreadLocalDemo2 demo2 = new ThreadLocalDemo2();
        ThreadLocalDemo3 demo3 = new ThreadLocalDemo3();
        System.out.println("========ThreadLocalDemo2==main====");
        System.out.println("threadLocal.get() = " + ThreadLocalUtils.get());

        ThreadLocalDemo2.m1();
        demo2.m2();

        ThreadLocalDemo3.m1();
        demo3.m2();

    }

    public static void m1() {
        System.out.println("========ThreadLocalDemo2==m1====");
        System.out.println("threadName = " + Thread.currentThread().getName() + "; traceId = " + ThreadLocalUtils.get());
    }

    public void m2() {
        System.out.println("========ThreadLocalDemo2==m2====");
        System.out.println("threadName = " + Thread.currentThread().getName() + "; traceId = " + ThreadLocalUtils.get());
    }
}
