package com.zzg.thread.queue;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * SynchronousQueue是一个特殊的BlockingQueue，
 * 它没有容量，没执行一个插入操作就会阻塞，需要再执行一个删除操作才会被唤醒，
 * 反之每一个删除操作也都要等待对应的插入操作。
 * 相当于一个长度为 0 的队列。
 */
public class SynchronousQueueDemo {

    public static void main(String[] args) {
        SynchronousQueue<String> queue = new SynchronousQueue<>();

        new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    queue.put(String.valueOf(i));
                    System.out.println(Thread.currentThread().getName() + "-> put queue i = " + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "AAA").start();

        new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    String res = queue.take();
                    System.out.println(Thread.currentThread().getName() + "-> take queue i = " + res);
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "BBB").start();

    }

}
