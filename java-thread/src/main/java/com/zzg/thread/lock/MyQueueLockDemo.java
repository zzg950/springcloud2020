package com.zzg.thread.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 传统模式编写
 * 生产者 --> 消费者模式 --> 手动编写一个生产者消费者模式
 * 题目 : 一个初始值为0的变量, 2个线程对其交替操作, 一个执行加1, 一个执行减1
 * <p>
 * 1. 线程-操控-资源类
 * 2. 判断-干活-唤醒
 * 3. 防止虚假唤醒
 * <p>
 * 1. 使用 Lock 锁
 * 2. 生产一个消费一个，模拟 SynchronousQueue
 */
public class MyQueueLockDemo {


    private int number = 0;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    /**
     * 1. 模拟生产者 - 使用Lock锁
     */
    public void increment() {
        // lock.lock() 后面必须跟 try ... catch ... finally, 且 lock.unlock() 必须在 finally 的第一行
        lock.lock();
        try {
            // 1. 判断  当number != 0说明存在被消费的资源，调用 await 等待 - 多线程情况下一定要用 while 判断
            while (number != 0) {
                condition.await();
            }
            // 2. 操作
            number++;
            System.out.println(Thread.currentThread().getName() + " --> number = " + number);

            // 3. 唤醒通知
            condition.signalAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    /**
     * 2. 模拟消费者 - 使用 Lock 锁
     */
    public void decrement() {
        // lock.lock() 后面必须跟 try ... catch ... finally, 且 lock.unlock() 必须在 finally 的第一行
        lock.lock();
        try {
            // 1. 判断
            while (number == 0) {
                condition.await();
            }
            // 2. 操作
            number--;
            System.out.println(Thread.currentThread().getName() + " --> number = " + number);

            // 3. 唤醒通知
            condition.signalAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }


    public static void main(String[] args) {
        MyQueueLockDemo myQueue = new MyQueueLockDemo();
        new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    myQueue.increment();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "AAA").start();

        new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    myQueue.decrement();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "BBB").start();
    }

}
