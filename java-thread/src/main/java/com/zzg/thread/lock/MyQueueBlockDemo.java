package com.zzg.thread.lock;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 传统模式编写
 * 生产者 --> 消费者模式 --> 手动编写一个生产者消费者模式
 * 题目 : 一个初始值为0的变量, 2个线程对其交替操作, 一个执行加1, 一个执行减1
 * <p>
 * 1. 线程-操控-资源类
 * 2. 判断-干活-唤醒
 * 3. 防止虚假唤醒
 * <p>
 * 1. 使用 ArrayBlockQueue 实现
 * 2. 生产一个消费一个，模拟 SynchronousQueue
 * <p>
 * 知识点: volatile/CAS/atomicInteger/BlockQueue/线程交互/原子引用
 */
public class MyQueueBlockDemo {

    private volatile boolean FLAG = true;
    private AtomicInteger atomicInteger = new AtomicInteger();
    private BlockingQueue<String> blockingQueue;

    public MyQueueBlockDemo(BlockingQueue<String> blockingQueue) {
        System.out.println("BlockingQueue --> " + blockingQueue.getClass().getName());
        this.blockingQueue = blockingQueue;
    }

    private void producer() throws InterruptedException {
        String data = "";
        boolean res;
        while (FLAG) {
            data = atomicInteger.incrementAndGet() + "";
            res = blockingQueue.offer(data, 2L, TimeUnit.SECONDS);
            if (res) {
                System.out.println(Thread.currentThread().getName() + "\t 插入队列 data = " + data + " 成功");
            } else {
                System.out.println(Thread.currentThread().getName() + "\t 插入队列 data = " + data + " 失败");
            }
            // 控制速度 - 一秒钟生产一个
            TimeUnit.SECONDS.sleep(1);
        }
        System.out.println(Thread.currentThread().getName() + "\t 生产者运行结束; FLAG = " + FLAG);
    }

    private void consumer() throws InterruptedException {
        String data = "";
        while (FLAG) {
            data = blockingQueue.poll(2L, TimeUnit.SECONDS);
            System.out.println(Thread.currentThread().getName() + "\t 消费队列 data = " + data);
        }
        System.out.println(Thread.currentThread().getName() + "\t 消费者运行结束; FLAG = " + FLAG);
    }

    private void stop() {
        this.FLAG = false;
        System.out.println("生产者-->消费者-->运行结束");
    }


    public static void main(String[] args) throws InterruptedException {
        MyQueueBlockDemo myQueue = new MyQueueBlockDemo(new ArrayBlockingQueue<>(1));

        new Thread(() -> {
            try {
                System.out.println("生产者-->启动成功");
                System.out.println();
                System.out.println();
                myQueue.producer();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "P").start();


        new Thread(() -> {
            try {
                System.out.println("消费者-->启动成功");
                System.out.println();
                System.out.println();
                myQueue.consumer();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "C").start();

        // 5秒钟之后 - 程序暂停
        TimeUnit.SECONDS.sleep(5L);
        myQueue.stop();
    }


}
