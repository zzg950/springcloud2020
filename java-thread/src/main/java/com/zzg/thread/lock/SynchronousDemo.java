package com.zzg.thread.lock;

/**
 * Synchronous 关键字修饰的锁示例
 * <p>
 * synchronized 修饰两个普通方法 - 或者一个普通方法，一个synchronized (this)的代码块 - 两个 synchronized (this) 修饰代码块
 * 1. 在多线程中使用同一个 SynchronousDemo 对象调用hello1和hello2，打印结果不会混乱，说明使用的是同一把锁
 * 2. 在多线程中使用不同的 SynchronousDemo 对象调用hello1和hello2，打印结果穿插会乱，说明使用的不是同一把锁
 * <p>
 * synchronized 修饰两个静态方法 - 或者一个静态方法，一个synchronized (this.getClass())修饰的代码块  - 两个 synchronized (this.getClass()) 修饰的代码块
 * 1. 在多线程中使用同一个 SynchronousDemo 对象调用静态方法hello1和hello2，打印结果不会混乱，说明使用的是同一把锁
 * 2. 在多线程中使用不同的 SynchronousDemo 对象调用测试
 * <p>
 * 结论：
 * synchronized 修饰的普通方法跟 synchronized (this) 一样，是当前对象的锁，多个对象有多个锁
 * synchronized 修饰的静态方法跟 synchronized (this.getClass()) 一样，在整个JVM中只有一把锁
 * synchronized (this) 和 synchronized (this.getClass()) 获取的锁不一样
 * synchronized 修饰的普通方法和 synchronized 修饰的静态方法获取的锁不一样
 */
public class SynchronousDemo {

    public void hello1() {
        synchronized (this.getClass()) {
            System.out.println("hello1 --- begin");
            System.out.println("hello1 --- " + Thread.currentThread().getName());
            System.out.println("hello1 --- end");
        }
    }

    public synchronized void hello2() {
        System.out.println("\t\t  hello2 --- begin");
        System.out.println("\t\t  hello2 --- " + Thread.currentThread().getName());
        System.out.println("\t\t  hello2 --- end");
    }

    public static void main(String[] args) {
        SynchronousDemo demo = new SynchronousDemo();
        SynchronousDemo demo1 = new SynchronousDemo();
        for (int i = 1; i <= 10; i++) {
            new Thread(() -> {
                demo.hello1();
                demo1.hello2();
            }, "Thread-Name-" + i).start();
        }
    }

}
