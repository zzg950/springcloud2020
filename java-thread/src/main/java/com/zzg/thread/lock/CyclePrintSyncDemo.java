package com.zzg.thread.lock;

import java.util.Arrays;
import java.util.List;

/**
 * 题目要求: 多线程之间按照顺序环形执行 A==>B==>C==>A==>B==>C
 * AA打印5次， BB打印10次，CC打印15次
 * ......
 * 运行5轮
 * 使用Synchronous循环抢占资源
 */

public class CyclePrintSyncDemo {

    private String conName = "A";

    private synchronized void println(String name, int count) throws InterruptedException {
        System.out.println("进入println程序-->name = " + name + "; count = " + count);
        // 1. 判断
        while (!conName.equalsIgnoreCase(name)) {
            this.wait();
        }
        // 2. 执行
        for (int i = 1; i <= count; i++) {
            System.out.println(Thread.currentThread().getName() + "\t" + name + "\t" + i);
        }
        // 3. 唤醒 -- 切换下一个可以运行的线程的标识 -- 然后唤醒所有线程让其抢夺，直到可运行的线程抢到为止
        this.conName = this.getNextName(name);
        this.notifyAll();
    }

    /**
     * 后期需改造成枚举类型
     *
     * @param name
     * @return
     */
    private int getPrintCount(String name) {
        if ("A".equalsIgnoreCase(name)) return 2;
        if ("B".equalsIgnoreCase(name)) return 2;
        if ("C".equalsIgnoreCase(name)) return 2;
        return 1;
    }

    private String getNextName(String name) {
        if ("A".equalsIgnoreCase(name)) return "B";
        if ("B".equalsIgnoreCase(name)) return "C";
        if ("C".equalsIgnoreCase(name)) return "A";
        return "";
    }

    public static void main(String[] args) {
        CyclePrintSyncDemo demo = new CyclePrintSyncDemo();
        /**
         * 定义三个线程，每个线程打印5轮，然后让由程序自动控制打印
         */
        List<String> list = Arrays.asList("A", "B", "C");
        for (String name : list) {
            new Thread(() -> {
                int count = demo.getPrintCount(name);
                for (int i = 0; i < 5; i++) {
                    try {
                        demo.println(name, count);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, name).start();
        }
    }

}
