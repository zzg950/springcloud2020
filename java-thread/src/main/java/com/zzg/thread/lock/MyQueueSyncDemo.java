package com.zzg.thread.lock;

/**
 * 传统模式编写
 * 生产者 --> 消费者模式 --> 手动编写一个生产者消费者模式
 * 题目 : 一个初始值为0的变量, 2个线程对其交替操作, 一个执行加1, 一个执行减1
 * <p>
 * 1. 线程-操控-资源类
 * 2. 判断-干活-唤醒
 * 3. 防止虚假唤醒
 * <p>
 * 1. 使用 synchronized 加锁
 * 2. 生产一个消费一个，模拟 SynchronousQueue
 */
public class MyQueueSyncDemo {

    private int number = 0;

    /**
     * 1. 模拟生产者
     */
    public synchronized void increment() throws Exception {
        // 1. 判断
        while (number != 0) {
            this.wait();
        }
        // 2. 操作
        number++;
        System.out.println(Thread.currentThread().getName() + " --> 消息生产 --> number = " + number);

        // 3. 唤醒通知
        this.notifyAll();
    }

    /**
     * 2. 模拟消费者
     *
     * @throws Exception
     */
    public synchronized void decrement() throws Exception {
        // 1. 判断
        while (number == 0) {
            this.wait();
        }
        // 2. 操作
        number--;
        System.out.println(Thread.currentThread().getName() + " --> 消息消费 --> number = " + number);

        // 3. 唤醒通知
        this.notifyAll();
    }

    public static void main(String[] args) {
        MyQueueSyncDemo myQueue = new MyQueueSyncDemo();

        /**
         * 开启2个线程，一个生产一个消费，运行5轮
         */
        new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    myQueue.increment();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "AAA").start();

        new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    myQueue.decrement();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "BBB").start();
    }

}
