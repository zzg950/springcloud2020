package com.zzg.thread.lock;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 读写锁
 * 编写一个缓存
 * 多个线程读取同一个资源类没有问题，所以为了满足并发量，读取资源应该是可以并行进行
 * 但是
 * 如果有一个线程想写共享资源，就不应该再由其他线程对该资源进行读或者写
 * 总结:
 * 读 - 读 可以并行
 * 读 - 写 不可并行
 * 写 - 写 不可并行
 */
public class ReadWriteLockDemo {

    private volatile Map<String, Object> cacheMap = new HashMap<>();
    private ReentrantReadWriteLock rwlock = new ReentrantReadWriteLock();

    public void put(String key, Object value) {
        rwlock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "~~~写入开始: " + key);
//            try { TimeUnit.MILLISECONDS.sleep(100); } catch (Exception e) { e.printStackTrace(); }
            cacheMap.put(key, value);
            System.out.println(Thread.currentThread().getName() + "~~~写入完成");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rwlock.writeLock().unlock();
        }
    }

    public void get(String key) {
        rwlock.readLock().lock();
        try {
//            System.out.println(Thread.currentThread().getName() + "~~~读取开始");
//            Object res = cacheMap.get(key);
//            System.out.println(Thread.currentThread().getName() + "~~~读取完成: key = " + key + "; value = " + res);
            System.out.println(cacheMap);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rwlock.readLock().unlock();
        }
    }

    public synchronized void syncPut(String key, Object value) {
        System.out.println(Thread.currentThread().getName() + "~~~写入开始: " + key);
//        try { TimeUnit.MILLISECONDS.sleep(100); } catch (Exception e) { e.printStackTrace(); }
        cacheMap.put(key, value);
        System.out.println(Thread.currentThread().getName() + "~~~写入完成");
    }

    public synchronized void syncGet(String key) {
        /**
         * 精确查找map对象
         * 这种调用方式不会报ConcurrentModificationException异常
         * 因为map.get()并不会遍历整个map对象，不关心集合长度是否发生变化
         System.out.println(Thread.currentThread().getName() + "~~~读取开始");
         Object res = cacheMap.get(key);
         System.out.println(Thread.currentThread().getName() + "~~~读取完成: key = " + key + "; value = " + res);
         */

        /**
         * 集合全部打印
         * println(map) 会调用 HashMap.toString()，它会遍历 EntrySet，
         * 而 EntrySet Iterator next 会检测数据变化，
         * 如果有变化就报 ConcurrentModificationException 了
         * 如果使用 for 循环自己编译也依然会报这个错误
         */
        System.out.println(Thread.currentThread().getName() + "~~~读取~~~" + cacheMap);
    }

    public static void main(String[] args) throws Exception {
        ReadWriteLockDemo demo = new ReadWriteLockDemo();
        for (int i = 1; i <= 50; i++) {
            final int tmpint = i;
            new Thread(() -> {
//                demo.put(tmpint + "", tmpint + "");
                demo.syncPut(tmpint + "", tmpint + "");
            }, "Thread-Name-" + i).start();
        }

        for (int i = 1; i <= 50; i++) {
            final int tmpint = i;
            new Thread(() -> {
//                demo.get(tmpint + "");
                demo.syncGet(tmpint + "");
            }, "Thread-Name-" + i).start();
        }

    }

}
