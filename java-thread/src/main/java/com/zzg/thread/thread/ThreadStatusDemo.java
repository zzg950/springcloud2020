package com.zzg.thread.thread;

import com.zzg.springcloud.common.util.PrintUtils;

import java.util.concurrent.TimeUnit;

/**
 * 线程的状态
 * 1. New - 新生状态 : 线程创建之后，调用 start() 方法之前
 * 2. Runnable - 准备状态 : 就绪状态，可运行状态，调用 start() 方法之后进行就绪状态， 此时等待JVM的调度
 * 3. Running - 运行状态 : 就绪状态的线程获取了CPU的资源，就可以执行run()方法，此时线程是处于运行状态。处于运行状态的线程比较复杂，
 * 它可以变成阻塞状态、就绪状态、死亡状态。
 * 4. Blocked - 阻塞状态 : 一个运行中的线程由于某种原因不能再继续运行时，就会进入阻塞状态。这是一个【不可运行】的状态，需要后续特定事件通知才会转入【可运行状态】
 * (1) 等待阻塞 : 运行状态中的线程调用 wait() 方法，使线程进入到等待阻塞状态。
 * (2) 同步阻塞 : 线程在获取 synchronized 同步锁失败时，锁被其他线程获取到了
 * (3) 其他阻塞 : 线程调用 sleep()、 join()方法或者发出了 I/O 请求时，线程就会进入到阻塞状态。
 * 当sleep超时，join等待结束，I/O处理完毕后，线程重新进入【就绪状态】
 * 5. Dead - 死亡状态 : 一个线程run()方法运行完毕，stop() 方法被调用，或者出现了未捕获的异常时，线程就会结束，进入死亡状态。
 * <p>
 * 新生状态 和 死亡状态 只能有一次，是不可逆的。
 * <p>
 * Thread 中有一个内部枚举 - 定义了线程的各个状态
 */
public class ThreadStatusDemo {

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread();
        PrintUtils.printThreadAndMessage("thread 新建: " + thread.getState());
        thread.start();
        PrintUtils.printThreadAndMessage("thread start 之后 : " + thread.getState());
        TimeUnit.SECONDS.sleep(5);
        PrintUtils.printThreadAndMessage("thread sleep 之后 : " + thread.getState());
    }

}
