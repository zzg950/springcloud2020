package com.zzg.thread.thread;

/**
 * 通过实现 Runnable 实现线程
 */
public class RunnableDemo implements Runnable {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RunnableDemo() {
    }

    public RunnableDemo(String status) {
        this.status = status;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "~~~come in runnable run method~~~status = " + getStatus());
    }

    public static void main(String[] args) {
        RunnableDemo demo = new RunnableDemo();
        Thread thread = new Thread(demo);
        thread.start();

        RunnableDemo demo1 = new RunnableDemo("123");
        Thread thread1 = new Thread(demo1);
        thread1.start();
    }
}
