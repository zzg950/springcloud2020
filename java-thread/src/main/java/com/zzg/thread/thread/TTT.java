package com.zzg.thread.thread;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.IntStream;

public class TTT {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        LinkedList<String> linkedList = new LinkedList<>();
        Queue<String> linkedQueue = new LinkedList<>();

        IntStream.range(1, 5).forEach(item -> {
            linkedQueue.add("add_" + item);
            linkedQueue.offer("offer_" + item);
            linkedList.add("add_" + item);
            linkedList.offer("offer_" + item);
        });
        System.out.println(linkedQueue);

        System.out.println("=================================");
        for (String str : linkedQueue) {
            System.out.println(str);
        }
        System.out.println("=================================");
        System.out.println(linkedList.get(5));

        System.out.println(linkedQueue.size() + " | " + linkedQueue.poll() + " | " + linkedQueue.size());
        System.out.println(linkedQueue.size());
        System.out.println(Integer.MAX_VALUE);

    }


}
