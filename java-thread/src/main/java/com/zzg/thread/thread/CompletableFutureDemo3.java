package com.zzg.thread.thread;

import com.zzg.springcloud.common.util.PrintUtils;
import com.zzg.springcloud.common.util.SleepUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 在CompletableFuture中使用线程池
 */
public class CompletableFutureDemo3 {

    // 设置一个无界队列的线程池
//    private static final ExecutorService threadPool = Executors.newCachedThreadPool();

    // 设置一个固定大小的线程池
    private static final ExecutorService threadPool = Executors.newFixedThreadPool(5);

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        List<CompletableFuture> futureList = new ArrayList<>();
        for (int i = 0; i < 150; i++) {
            final int tempInt = i;
            CompletableFuture future = CompletableFuture.runAsync(() -> {
                SleepUtils.sleepMilliseconds(1000);
                PrintUtils.printThreadAndMessage("Thread-" + tempInt + " 处理完成。" + System.currentTimeMillis());
            }, threadPool);
            futureList.add(future);
        }

        // 等待所有线程执行完毕
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()])).join();
        PrintUtils.printThreadAndMessage("所有任务已经处理完成，总耗时 : " + (System.currentTimeMillis() - start));

        // 线程池是一个前台线程，需要手动关闭，不然程序不会停止
        // 可以通过改造，处理成守护线程，等程序执行完毕后自动关闭
        threadPool.shutdown();
    }


}
