package com.zzg.thread.thread;


import com.zzg.springcloud.common.util.PrintUtils;
import com.zzg.springcloud.common.util.SleepUtils;

/**
 * 实现线程的方法之一 : 实现 Runnable 接口
 * 1. Runnable 是没有返回值的
 * 2. Runnable 是一个函数式接口，可以直接使用函数式接口实现
 */
public class RunnableDemo2 {

    public static void main(String[] args) throws Exception {
        /**
         * 第一种方法: 创建 runnable 接口对象实现线程
         */
        PrintUtils.printThreadAndMessage("1. 我是main线程 - 开始。");
        Runnable runnable = () -> {
            PrintUtils.printThreadAndMessage("2. 我是runnable创建的子线程 - 开始。");
            SleepUtils.sleepMilliseconds(100, 200);     // 子线程创建后睡眠100-200ms
            PrintUtils.printThreadAndMessage("3. 我是runnable创建的子线程 - 结束");
        };
        Thread thread = new Thread(runnable, "runnableInterfaceThread");
        thread.start();
        thread.join();      // 等待线程执行完毕后，再执行后面代码
        PrintUtils.printThreadAndMessage("4. 我是main线程 - 第一部分完结。撒花~~~");

        System.out.println("---------------------------------------------------------------");
        /**
         * 第二种方法: 直接在thread构造函数中实现接口
         */
        PrintUtils.printThreadAndMessage("2.1 我是main线程 - 我又开始。");
        Thread thread1 = new Thread(() -> {
            PrintUtils.printThreadAndMessage("2.2 我是runnable创建的子线程 - 开始。");
            SleepUtils.sleepMilliseconds(100, 200);     // 子线程创建后睡眠100-200ms
            PrintUtils.printThreadAndMessage("2.3 我是runnable创建的子线程 - 结束");
        }, "runnableInterfaceThread2");
        thread1.start();
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        PrintUtils.printThreadAndMessage("2.4 我是main线程 - 第二部分完结。撒花~~~");
    }

}
