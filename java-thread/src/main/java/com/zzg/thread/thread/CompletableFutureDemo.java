package com.zzg.thread.thread;


import com.zzg.springcloud.common.util.SleepUtils;

import java.util.concurrent.CompletableFuture;


/**
 * 开启一个带返回值的异步任务
 * <p>
 * CompletableFuture 的方法
 * <p>
 * supplyAsync 表示开启一个异步任务
 * thenCompose 表示连接两个任务，依赖关系
 * thenCombine 表示合并两个任务
 * applyToEither 比较两个任务结果，谁先执行完用谁的结果
 * exceptionally 异常处理
 */
public class CompletableFutureDemo {

    public static void main(String[] args) {
//        simpleExample();
        hardExample();
//        hellExample();
//        deadExample();
    }

    /**
     * 简单模式 - supplyAsync - 启动异步任务
     * 一个人到饭店点餐，等待出餐，等待期间执行任务的例子
     * <p>
     * 示例代码中的步骤三和步骤四谁先执行并不一定
     */
    public static void simpleExample() {
        System.out.println("1. 小明到饭店。" + getThreadInfo());
        System.out.println("2. 小明点餐结束 - 蛋炒饭" + getThreadInfo());

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("3. 厨师开始做饭" + getThreadInfo());
            SleepUtils.sleepMilliseconds(3000, 4000);   // 厨师做饭时间， 耗时: 3 - 4 秒之间
            return "蛋炒饭";
        });

        System.out.println("4. 小明等待期间开始玩手机游戏" + getThreadInfo());
        String res = completableFuture.join();
        System.out.println("5. 小时开始吃饭: " + res + getThreadInfo());
    }

    /**
     * 困难模式 - thenCompose - 两个任务链接，后面的任务需要等到前面任务结束后才执行
     * 小明到饭店点餐、厨师做饭、期间小明玩游戏，厨师做好之后服务员配置餐具送给小明、小明开始吃饭
     * <p>
     * supplyAsync: 表示异步执行一个任务, 另外起一个线程执行
     * thenCompose: 表示前一个任务执行完成后，开始后面的任务，
     * 两个参数，result 是前一个任务返回的结果，function 表示要执行的任务
     * thenCompose、 thenComposeAsync 的区别: thenCompose 使用前一个线程继续执行，thenComposeAsync 异步，新起线程执行。
     * <p>
     * thenCompose、 thenComposeAsync、thenApply、 thenApplyAsync 功能相似，暂时只是看到写法不同而已
     * thenXXXX 在原线程上接着执行
     * thenXXXXAsync 是新起一个线程执行
     * <p>
     * 示例代码中的步骤3和步骤6谁先执行，并不是一定的，看谁先抢到线程
     */
    public static void hardExample() {
        System.out.println("1. 小明到饭店。" + getThreadInfo());
        System.out.println("2. 小明点餐结束 - 蛋炒饭。" + getThreadInfo());

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("3. 厨师开始做饭。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(3000, 4000);   // 厨师做饭时间， 耗时: 3 - 4 秒之间
            System.out.println("4. 厨师饭已做好。" + getThreadInfo());
            return "蛋炒饭";
        }).thenComposeAsync(result -> CompletableFuture.supplyAsync(() -> {
            System.out.println("5. 服务员开始出餐 + 配置餐具。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(500, 1000);   // 服务员端饭时间， 耗时: 0.5 - 1 秒之间
            return result + " + 餐具。";
        }));

        System.out.println("6. 小明等待期间开始玩手机游戏。" + getThreadInfo());
        String res = completableFuture.join();
        System.out.println("7. 小时开始吃饭: " + res + getThreadInfo());
    }

    /**
     * 困难模式 - thenApply - 两个任务链接，类似thenCompose，后一个任务需要依赖前一个任务执行结果
     * 使用 thenApply 或者 thenApplyAsync 代替 thenCompose 和 thenComposeAsync
     */
    public static void hardExampleThenApply() {
        System.out.println("1. 小明到饭店。" + getThreadInfo());
        System.out.println("2. 小明点餐结束 - 蛋炒饭。" + getThreadInfo());

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("3. 厨师开始做饭。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(3000, 4000);   // 厨师做饭时间， 耗时: 3 - 4 秒之间
            System.out.println("4. 厨师饭已做好。" + getThreadInfo());
            return "蛋炒饭";
        }).thenApplyAsync(result -> {
            System.out.println("5. 服务员开始出餐 + 配置餐具。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(500, 1000);   // 服务员端饭时间， 耗时: 0.5 - 1 秒之间
            return result + " + 餐具";
        });

        System.out.println("6. 小明等待期间开始玩手机游戏。" + getThreadInfo());
        String res = completableFuture.join();
        System.out.println("7. 小时开始吃饭: " + res + getThreadInfo());
    }


    /**
     * 地狱模式 - thenCombine - 合并模式，两个任务合并执行，同时执行，最后等2个任务执行完成后合并结果返回
     * 小明到饭店点餐、厨师炒菜、服务员蒸米饭、小明玩游戏，等炒菜结束 + 蒸饭结束，合并上饭菜，小明开始吃饭
     * <p>
     * thenCombine: 表示要跟上一个任务同时开始执行，接受两个参数，第一个参数是当前任务要执行的方法内容，第二个参数是BiFunction，整合2个异步方法的结果进行处理
     * cai: 是第一个方法返回内容， 炒菜
     * mi:  是第二个方法返回内容， 米饭
     * <p>
     * 如果要同时执行3个步骤怎么处理: 炒菜 + 蒸米饭 + 配置餐具 同时执行呢？
     * <p>
     * 示例代码中，步骤3和步骤8执行顺序不确定，步骤4和步骤6执行顺序不确定，依赖谁先执行完毕
     */
    public static void hellExample() {
        System.out.println("1. 小明到饭店。" + getThreadInfo());
        System.out.println("2. 小明点餐结束 - 鸡蛋番茄盖浇饭。" + getThreadInfo());

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("3. 厨师开始炒菜。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(3000, 4000);   // 厨师炒菜时间， 耗时: 3 - 4 秒之间
            System.out.println("4. 厨师菜已炒好。" + getThreadInfo());
            return "鸡蛋番茄";
        }).thenCombine(CompletableFuture.supplyAsync(() -> {
            System.out.println("5. 服务员开始蒸米饭。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(500, 1000);   // 服务员蒸饭时间， 耗时: 3 - 4 秒之间
            System.out.println("6. 服务员米饭已蒸好。" + getThreadInfo());
            return "米饭";
        }), (cai, mi) -> {
            System.out.println("7. 服务员开始打饭 + 配置餐具。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(500, 1000);   // 服务员打饭时间， 耗时: 0.5 - 1 秒之间
            return "鸡蛋番茄 + 米饭 + 餐具";
        });

        System.out.println("8. 小明等待期间开始玩手机游戏。" + getThreadInfo());
        String res = completableFuture.join();
        System.out.println("9. 小时开始吃饭: " + res + getThreadInfo());
    }


    /**
     * 死亡模式
     * 小明在等待公交车，有100路和200路可以回家，谁先来就坐哪一趟车，如何实现等待的过程
     * <p>
     * applyToEither: 表示两个任务谁先执行完就返回谁的结果, firstComBus 就是返回结果，同时在返回的时候可以对结果进行预处理
     * <p>
     * exceptionally: 异常处理-->前面发生异常时需要执行的内容，相当于 cache 代码块
     * exceptionally 不仅仅可以加在尾部，也可以在中间进行异常处理
     */
    public static void deadExample() {
        System.out.println("1. 小明在等公交车。" + getThreadInfo());

        CompletableFuture<String> waitBusFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("2. 100路公交车在过来的路上。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(2000, 4000);
            System.out.println("3. 100路公交车到了。" + getThreadInfo());
            return "100路公交车";
        }).applyToEither(CompletableFuture.supplyAsync(() -> {
            System.out.println("4. 200路公交车在过来的路上。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(2000, 4000);
            System.out.println("5. 200路公交车到了。" + getThreadInfo());
            return "200路公交车";
        }), firstComeBus -> {
            if (firstComeBus.startsWith("100")) {
                throw new RuntimeException("100路公交抛锚了出问题了。");
            }
            return firstComeBus;
        }).exceptionally(e -> {
            System.out.println("Exception message = " + e.getMessage() + getThreadInfo());
            System.out.println("7. 小明开始叫出租车。" + getThreadInfo());
            SleepUtils.sleepMilliseconds(2000, 4000);
            return "出租车";
        });

        String res = waitBusFuture.join();
        System.out.println("8. 小明坐上回家的车: " + res + getThreadInfo());

    }


    public static String getThreadInfo() {
        return "threadId = " + Thread.currentThread().getId() + "; threadName = " + Thread.currentThread().getName();
    }

}
