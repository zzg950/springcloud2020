package com.zzg.jvm.gc;

import java.util.concurrent.TimeUnit;

/**
 * 一个普通GC回收的demo
 */
public class GCHelloDemo {

    public static void main(String[] args) throws Exception {
        System.out.println(System.getProperty("java.vm.name"));
        System.out.println(System.getProperty("sun.java.command"));
        System.out.println(System.getProperty("NewSize"));
        TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
    }

}
