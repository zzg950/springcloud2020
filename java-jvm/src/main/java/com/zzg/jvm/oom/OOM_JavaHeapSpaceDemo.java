package com.zzg.jvm.oom;


import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * java.lang.OutOfMemoryError: Java heap space
 * 堆内存溢出
 * 堆内存空间不足以存放新产生的对象
 * <p>
 * 把堆内存设置小一点，不停的创建字符串常量，然后 intern 到常量池，最终会报 Java heap space 的OOM
 * 由此可以证明，在JDK7和JDK8之后，字符串常量池存储在堆内存中
 * <p>
 * 但是在JDK6的虚拟机上实验的话，则会报 PremGen 的OOM异常，说明之前的版本字符串常量池是在永久代中
 * <p>
 * 1. 在 IDEA 中设置堆内存大小，点击 Edit Configurations --> VM options
 * --> 需要先让即将运行的类执行一遍，保证 Main.class 只要被设置的类
 * --> 填写启动参数 -Xms10M -Xmx10M -XX:+PrintGCDetails --> 保存
 */
public class OOM_JavaHeapSpaceDemo {


    public static void main(String[] args) {
        example1();
    }

    /**
     * 为了方便演示，将堆内存空间设置小一点，打印了GC日志，
     * 可以明确看到到后来就是一只在进行FULL GC，最终因为str太大，导致整个堆都无法存储导致了 OOM
     * -Xms10M -Xmx10M -XX:+PrintGCDetails
     * Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
     */
    public static void example1() {
        System.out.println("Runtime.getRuntime().availableProcessors() = " + Runtime.getRuntime().availableProcessors());
        // 直接NEW一个大对象 - 直接创建一个10M的对象
//        byte[] bytes = new byte[10 * 1024 * 1024];
        StringBuffer sb = new StringBuffer(10 * 1024 * 1024);
        StringBuilder sbu = new StringBuilder(10 * 1024 * 1024);
    }

    /**
     * -Xms10M -Xmx10M -XX:+PrintGCDetails
     * 通过不停的创建字符串常量，然后 intern 到字符串常量池，撑爆堆空间，导致 Java heap space 的 OOM
     * 以此证明在当前版本的字符串常量池是在堆中。
     * 但是：此方法貌似无法证明是常量池满了导致的OOM，还是堆空间的老年代满了导致的OOM
     */
    public static void example2() {
        List<String> list = new ArrayList<>();
        int index = 0;
        String uuid = UUID.randomUUID().toString().intern();
        try {
            while (true) {
                uuid += UUID.randomUUID().toString().intern();
                list.add(uuid);
                index++;
            }
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            System.out.println("index = " + index);
        }
    }


}
