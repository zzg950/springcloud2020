package com.zzg.jvm.oom;


import java.nio.ByteBuffer;

/**
 * java.lang.OutOfMemoryError: Direct buffer memory
 * 堆外直接内存溢出
 * 堆空间以外的内存使用过多，导致内存溢出
 * 在使用NIO的时候
 */
public class OOM_DirectBufferMemoryDemo {

    /**
     * -XX:MaxDirectMemorySize 堆外直接内存最大使用量
     * 配置参数: -Xms10M -Xmx10M -XX:+PrintGCDetails -XX:MaxDirectMemorySize=5M
     */
    public static void main(String[] args) {
        long maxDirectMemory = sun.misc.VM.maxDirectMemory() / 1024 / 1024;
        System.out.println("配置的maxDirectMemory大小: " + maxDirectMemory + "MB");
        ByteBuffer bytes = ByteBuffer.allocateDirect(6 * 1024 * 1024);
    }

}
