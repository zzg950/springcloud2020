package com.zzg.jvm.demo;


import com.zzg.springcloud.common.entity.Student;

public class MemoryDemo {

    private static boolean wait = false;
    private static String str = "123";
    private static int mm = 1;
    private static Student student = new Student();

    public static void main(String[] args) {
        System.out.println("====================String Memory Path====================");
        System.out.println(str + "-->hashcode-->" + str.hashCode() + "-->memorypath-->" + System.identityHashCode(str));
        str = "456";
        System.out.println(str + "-->hashcode-->" + str.hashCode() + "-->memorypath-->" + System.identityHashCode(str));
        str = "123";
        System.out.println(str + "-->hashcode-->" + str.hashCode() + "-->memorypath-->" + System.identityHashCode(str));

        System.out.println("====================boolean Memory Path====================");
        System.out.println(wait + "-->hashcode-->" + "wait.hashCode()" + "-->memorypath-->" + System.identityHashCode(wait));
        wait = true;
        System.out.println(wait + "-->hashcode-->" + "wait.hashCode()" + "-->memorypath-->" + System.identityHashCode(wait));
        wait = false;
        System.out.println(wait + "-->hashcode-->" + "wait.hashCode()" + "-->memorypath-->" + System.identityHashCode(wait));

        System.out.println("====================int Memory Path====================");
        System.out.println(mm + "-->hashcode-->" + "wait.hashCode()" + "-->memorypath-->" + System.identityHashCode(mm));
        mm = 100;
        System.out.println(mm + "-->hashcode-->" + "wait.hashCode()" + "-->memorypath-->" + System.identityHashCode(mm));
        mm = 1;
        System.out.println(mm + "-->hashcode-->" + "wait.hashCode()" + "-->memorypath-->" + System.identityHashCode(mm));

        System.out.println("====================StudentDemo Memory Path====================");
        System.out.println(student + "-->hashcode-->" + student.hashCode() + "-->memorypath-->" + System.identityHashCode(student));
        student = new Student();
        System.out.println(student + "-->hashcode-->" + student.hashCode() + "-->memorypath-->" + System.identityHashCode(student));
        student = new Student();
        System.out.println(student + "-->hashcode-->" + student.hashCode() + "-->memorypath-->" + System.identityHashCode(student));
    }

}
