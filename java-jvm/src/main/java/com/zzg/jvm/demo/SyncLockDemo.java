package com.zzg.jvm.demo;

public class SyncLockDemo {

    private static int mm = 0;

    public static void main(String[] args) {
        Object obj = new Object();
        for (int i = 0; i < 20; i++) {
            final Integer tmpint = i;
            new Thread(() -> {
                synchronized (tmpint) {
                    for (int j = 0; j < 1000; j++) {
                        mm++;
                    }
                }
            }, "Thread-Name-" + i).start();
        }

        // 默认运行的有 main 和 GC 线程， 除此之外说明还有其他线程正在运行
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println(System.currentTimeMillis() + "-->" + Thread.currentThread().getName() + "-->SyncLockDemo end-->mm=" + mm);
    }

}
