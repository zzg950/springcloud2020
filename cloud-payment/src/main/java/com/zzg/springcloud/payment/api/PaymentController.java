package com.zzg.springcloud.payment.api;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import com.netflix.discovery.DiscoveryManager;
import com.zzg.springcloud.common.result.ReturnResult;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("payment")
@Slf4j
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${eureka.client.service-url.defaultZone}")
    private String eurekaBaseUrl;

    @Resource
    private DiscoveryClient discoveryClient;

    @Resource
    private HttpServletRequest request;

    // http://127.0.0.1:9091/payment/get/123
    @RequestMapping(value = "/get/{id}")
    public ReturnResult get(@PathVariable("id") String id) {
        System.out.println(this.getClass().getName() + "-->get : id = " + id);
        log.info("className = {}, methodName = {}, requestUrl = {}, threadName = {}, id = {}", this.getClass().getSimpleName(), "get", request.getRequestURL(), Thread.currentThread().getName(), id);
        JSONObject res = this.getReturnTemplate();
        res.put("id", id);
        res.put("request.type", "get");
        res.put("name", "张志刚");
        return ReturnResult.get(res);
    }

    // http://127.0.0.1:9091/payment/post/123
    @PostMapping(value = "/post/{id}")
    public ReturnResult post(@PathVariable("id") String id, @RequestBody JSONObject params) {
        System.out.println(this.getClass().getName() + "-->post : id = " + id + "; params = " + params.toJSONString());
        log.info("className = {}, methodName = {}, requestUrl = {}, id = {}, params = {}", this.getClass().getSimpleName(), "post", request.getRequestURL(), id, params.toJSONString());
        JSONObject res = this.getReturnTemplate();
        res.put("id", id);
        res.put("request.type", "post");
        res.put("params", params);
        log.info("responseBody = {}", res.toJSONString());
        return ReturnResult.get(res);
    }

    /**
     * 此方法获取到的是当前客户端缓存的注册信息
     * 因为:
     * 1. eureka server 关停后接口仍然可以获取注册信息
     * 2. 其他 eureka client 关停后仍然可以获取相关注册信息
     * 3. eureka server 关停的情况下, 重启当前服务之后无法再获取注册信息
     */
    // http://127.0.0.1:9091/payment/discovery
    @RequestMapping(value = "/discovery")
    public ReturnResult discovery() {
        log.info("className = {}, methodName = {}, requestUrl = {}", this.getClass().getSimpleName(), "discovery", request.getRequestURL());
        JSONObject result = new JSONObject();
        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            List<ServiceInstance> instances = discoveryClient.getInstances(service);
            result.put(service, instances);
        }
        return ReturnResult.get(result);
    }

    /**
     * 此方法获取到的是 Eureka Server 实时注册信息
     * 因为:
     * 1. 直接通过 Eureka Server 的 Rest API 方式获取的信息
     */
    // http://127.0.0.1:9091/payment/services/get
    @RequestMapping(value = "/services/get")
    public ReturnResult getServices() {
        log.info("className = {}, methodName = {}, requestUrl = {}", this.getClass().getSimpleName(), "getServices", request.getRequestURL());
        String url = eurekaBaseUrl + "apps";
        String res = HttpRequest.get(url).execute().body();
        JSONObject resJSON = JSONObject.parseObject(res);
        log.info("responseBody = {}", resJSON.toJSONString());
        return ReturnResult.get(resJSON);
    }

    /**
     * 服务下线
     * 1. 从客户端发送请求下线当前服务
     */
    // http://127.0.0.1:9091/payment/sleep/3000
    @GetMapping(value = "/sleep/{million}")
    public ReturnResult sleep(@PathVariable("million") Long million) throws InterruptedException {
        log.info("className = {}, methodName = {}, threadName = {}, requestUrl = {}", this.getClass().getSimpleName(), "sleep", Thread.currentThread().getName(), request.getRequestURL());
        JSONObject res = this.getReturnTemplate();
        Long start = System.currentTimeMillis();
        Thread.sleep(million);
        Long end = System.currentTimeMillis();
        res.put("cost", end - start);
        res.put("million", million);
        log.info("responseBody = {}", res.toJSONString());
        return ReturnResult.get(res);
    }

    /**
     * 服务下线
     * 1. 从客户端发送请求下线当前服务
     */
    // http://127.0.0.1:9091/payment/sleep2?million=123
    @GetMapping(value = "/sleep2")
    public ReturnResult sleep2(Long million) throws InterruptedException {
        log.info("className = {}, methodName = {}, threadName = {}, requestUrl = {}", this.getClass().getSimpleName(), "sleep2", Thread.currentThread().getName(), request.getRequestURL());
        JSONObject res = this.getReturnTemplate();
        Long start = System.currentTimeMillis();
        Thread.sleep(million);
        Long end = System.currentTimeMillis();
        res.put("cost", end - start);
        res.put("million", million);
        log.info("responseBody = {}", res.toJSONString());
        return ReturnResult.get(res);
    }

    /**
     * 服务下线
     * 1. 直接调用 Eureka Server 的 Rest API 下线指定的服务
     * 2. 下线之后如果 Eureka Client 持续保持心跳, 则会自动重新上线
     */
    // http://127.0.0.1:9091/payment/offline
    // http://127.0.0.1:9091/payment/offline/payment-cloud/192.168.0.109-cloud-payment:9091
    @GetMapping(value = "/offline/{applicationName}/{instanceId}")
    public ReturnResult offline(@PathVariable("applicationName") String applicationName, @PathVariable("instanceId") String instanceId) {
        log.info("className = {}, methodName = {}, threadName = {}, requestUrl = {}", this.getClass().getSimpleName(), "offline", Thread.currentThread().getName(), request.getRequestURL());
        String url = eurekaBaseUrl + "apps/" + applicationName + "/" + instanceId;
        log.info("url = {}", url);
        String res = HttpRequest.delete(url).execute().body();
        log.info("res = " + res);
        JSONObject resJSON = JSONObject.parseObject(res);
        return ReturnResult.get(resJSON);
    }

    /**
     * 服务下线
     * 1. 从客户端发送请求下线当前服务
     */
    // http://127.0.0.1:9091/payment/offline
    @GetMapping(value = "/offline")
    public ReturnResult offline() {
        log.info("className = {}, methodName = {}, requestUrl = {}", this.getClass().getSimpleName(), "offline", request.getRequestURL());
        DiscoveryManager.getInstance().shutdownComponent();
        return ReturnResult.get();
    }


    private JSONObject getReturnTemplate() {
        JSONObject res = new JSONObject(new TreeMap<>());
        res.put("className", this.getClass().getName());
        res.put("server.port", serverPort);
        res.put("spring.application.name", applicationName);
        res.put("payment.thread.name", Thread.currentThread().getName());
        res.put("payment.thread.id", Thread.currentThread().getId());
        res.put("payment.thread.state", Thread.currentThread().getState());
        res.put("requestId", UUID.randomUUID().toString());
        return res;
    }

}
