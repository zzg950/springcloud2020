package com.zzg.spring.annotation.service;


import com.zzg.spring.util.ContextUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.awt.image.renderable.ContextualRenderedImageFactory;

/**
 * 一个简单的Service，用于其他地方单独测试，自己不注入
 */
@Component
public class SimpleService implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public void test() {
        ContextUtils.showContextInfo(applicationContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
