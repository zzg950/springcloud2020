package com.zzg.spring.annotation.service;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * beanName = userService2222222222222222222222222
 */
@Service("userService2222222222222222222222222")
public class UserService {

    private String name;

    private OrderService orderService;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void testUser() {
        System.out.println("name = " + name);
    }
}
