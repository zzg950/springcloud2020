package com.zzg.spring.annotation;


import com.zzg.spring.annotation.config.AppConfig;
import com.zzg.spring.annotation.config.MyFactoryBean;
import com.zzg.spring.annotation.properties.PropertyComponent;
import com.zzg.spring.annotation.service.OrderService;
import com.zzg.spring.annotation.service.SimpleService;
import com.zzg.spring.annotation.service.UserService;
import com.zzg.spring.util.ContextUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

/**
 * 使用注解方式启动spring, 使用 @Bean 来描述 bean 信息
 */
public class AnnotationApplicationContext {

    public static void main(String[] args) throws IOException {
        // 可以在new AnnotationConfigApplicationContext()的构造方法中传入多个参数
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        ContextUtils.showContextInfo(context);

        PropertyComponent propertyComponent = context.getBean(PropertyComponent.class);
        System.out.println("propertyComponent.testValue() = " + propertyComponent.testValue());
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        AppConfig appConfig = context.getBean("appConfig", AppConfig.class);
        System.out.println("appConfig = " + appConfig);

        UserService userService = context.getBean("getUserService", UserService.class);
        System.out.println("userService.getName() = " + userService.getName());
        userService.testUser();

        /**
         * context.getBeanFactory() 此方法是 AnnotationConfigApplicationContext 才有的，
         * 所以要么最上面直接使用对象获取，要么在下面使用强制转换
         *
         * @Scope("prototype") 和 @Lazy 修饰的bean在容器启动之后，并不在单例池中，但是属于bean
         * 所以下面两行打印，第一行是true，第二行是false
         */
        AnnotationConfigApplicationContext context1 = (AnnotationConfigApplicationContext)context;
        System.out.println(context1.getBeanFactory().containsBean("orderService"));
        System.out.println(context1.getBeanFactory().containsSingleton("orderService"));


        OrderService orderService = context.getBean("orderService", OrderService.class);
        System.out.println("orderService.getName() = " + orderService.getName());

        System.out.println(context.getBean("orderService", OrderService.class));
        System.out.println(context.getBean("orderService", OrderService.class));
        System.out.println(context.getBean("orderService", OrderService.class));

        System.out.println(context.getBean(OrderService.class).getName());

        System.out.println(context.getBean("myFactoryBean"));
        System.out.println(context.getBean("myFactoryBean"));
        System.out.println(context.getBean(MyFactoryBean.class));

        SimpleService simpleService = context.getBean(SimpleService.class);
        simpleService.test();

        System.out.println("~~~~~~~~~~~~~国际化~~~~~~~~~~~~~~~~~~~~~~~");
        MessageSource messageSource = context.getBean(MessageSource.class);
        System.out.println("messageSource = " + messageSource);
        String message1 = messageSource.getMessage("name1", null, new Locale("en"));
        System.out.println("message1 = " + message1);

        String message = context.getMessage("name", null, new Locale("en"));
        System.out.println("message = " + message);

        Locale locale = LocaleContextHolder.getLocale();
        System.out.println(locale);
        System.out.println("locale.getLanguage() = " + locale.getLanguage());
        System.out.println("Locale.CHINESE = " + Locale.CHINESE);
        System.out.println("Locale.ENGLISH = " + Locale.ENGLISH);

        System.out.println("======================资源文件=======================");
        Resource resource = context.getResource("http://www.baidu.com");
        System.out.println("resource.contentLength() = " + resource.contentLength());
        System.out.println("resource.getDescription() = " + resource.getDescription());
        System.out.println("resource.getFilename() = " + resource.getFilename());
        System.out.println("resource.getURL() = " + resource.getURL());

        System.out.println("context.getEnvironment() = " + context.getEnvironment());
        System.out.println("context.getEnvironment().getDefaultProfiles().length = " + context.getEnvironment().getDefaultProfiles().length);
    }

}
