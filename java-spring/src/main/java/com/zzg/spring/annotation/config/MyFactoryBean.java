package com.zzg.spring.annotation.config;

import com.zzg.spring.annotation.service.SimpleService;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;


/**
 * 注册bean的一种方式
 * 1. 自定义一个MyFactoryBean类，实现FactoryBean接口
 * 2. getObject()方法返回的内容就是要注册的bean
 * 3. 需要使用 @Component 对 MyFactoryBean 进行修饰
 * 4. 默认产生的 beanName = myFactoryBean，但是对应的实际值是 SimpleService
 * 5. System.out.println(context.getBean("myFactoryBean"));   根据名字找到的是 SimpleService 的 Bean 对象
 * 6. System.out.println(context.getBean(MyFactoryBean.class));  根据类型找到的是 MyFactoryBean 的 Bean 对象
 */
@Component
public class MyFactoryBean implements FactoryBean {

    @Override
    public Object getObject() throws Exception {
//        return new SimpleService();
        return null;
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
