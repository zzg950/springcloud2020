package com.zzg.spring.annotation.config;


import com.zzg.spring.annotation.service.OrderService;
import com.zzg.spring.annotation.service.UserService;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;


/**
 * excludeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Component.class)
 * type = FilterType.ANNOTATION 表示通过注解的方式剔除内容
 * value = Component.class 或者 value = {Component.class, Controller.class}  表示要剔除 Component 和 Controller 标识的注解，也可以是自定义的注解
 *
 * includeFilters : 可以使用自定义的注解来标识一个bean
 * includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = {MyScanAnnotation.class}
 */
@ComponentScan(
        basePackages = {"com.zzg.spring.annotation"},
//        excludeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = {Component.class, Controller.class}),
        includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = {MyScanAnnotation.class})
)
@PropertySource(value = "classpath:spring-dev.properties", encoding = "UTF-8")
@Configuration      // 使用 @Configuration 修饰和不使用 @Configuration 修饰的区别
//@Import(UserService.class)      // 可以将一个普通类通过 Import 方式注册为一个 bean, 如果spring容器中包含此类型的bean，则不会再注册，如果不包含则会注册，beanName = 类的全限定名称
//@ImportResource("classpath:applicationContext-simple.xml")   // 通过 ImportResource 引入定义bean的 xml 文件
public class AppConfig {


    /**
     * 使用 @Bean 来定义要被扫描的 bean
     * 方法名 getUserService 将作为 beanName 存储在spring容器中
     */
    @Bean
    public UserService getUserService() {
        UserService userService = new UserService();
        userService.setName("user1");
        return userService;
    }


    @Bean
    @Primary
    public OrderService orderService() {
        OrderService orderService = new OrderService();
        orderService.setName("orderService121212");
        return orderService;
    }

    @Bean
    public OrderService getOrderService2() {
        OrderService orderService = new OrderService();
        orderService.setName("getOrderService2");
        return orderService;
    }

    /**
     * Spring 国际化使用
     * 1. 定义配置文件
     * 2. 将 MessageResource 定义成 bean
     */
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

        // 设置basename : 资源文件的名称前缀 messages.properties  messages_zh.properties (_后面是 locale 的语言，系统根据语言拼接具体读取的文件)
        messageSource.setBasename("messages");
//        messageSource.setBasename("i18n/messages");  // 如果文件不是在classpath跟目录下，则可主动设置所在目录地址

        // if true, the key of the message will be displayed if the key is not found, instead of throwing a NoSuchMessageException
        messageSource.setUseCodeAsDefaultMessage(true);

        // 解决从properties中读取数据中文乱码问题 - 默认是 : ISO-8859-1
        messageSource.setDefaultEncoding("UTF-8");

        // 是否缓存 : -1 --> never reload（spring加载之后文件修改也不变更，默认是-1）,
        //           0 --> always reload（每次读取都到文件中读取，只要文件变更，获取的结果也实时变更）
        //           10 --> cache time （缓存时间，相当于10秒读取重新缓存一次配置文件）
        messageSource.setCacheSeconds(10);

        return messageSource;
    }

}
