package com.zzg.spring.annotation.service;


import com.zzg.spring.annotation.config.MyScanAnnotation;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 使用自定义的注解 : 在@ComponentScan里面配置要扫描成bean的内容，将自定义的注解也扫描成一个bean
 */
@MyScanAnnotation
public class MyScanService {

    @Resource
    private OrderService orderService;

    @PostConstruct
    public void initMethod() {
        System.out.println("MyScanService~~~initMethod~~~" + orderService.getName());
    }

}
