package com.zzg.spring.annotation.service;


import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.UUID;

/**
 * beanName = orderService
 */
@Service
//@Scope("prototype")    //原型bean的话，spring容器启动时，不执行@PostConstruct修饰的初始化方法
//@Lazy                  //延迟加载，spring容器启动时，不执行@PostConstruct修饰的初始化方法，在第一次获取bean对象时执行。
public class OrderService {

    private String name = "orderService";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void testOrder() {
        System.out.println("name = " + name);
    }

    @PostConstruct
    public void initMethod() {
        System.out.println("OrderService~~~initMethod~~~" + name);
    }
}
