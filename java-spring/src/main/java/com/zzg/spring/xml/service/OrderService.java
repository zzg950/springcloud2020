package com.zzg.spring.xml.service;

/**
 * 定义接口 - 接口中的方法都是
 * 1. 接口中属性和方法不需要指定修饰符，默认是public
 * 2. 方法可以定义成 default 修饰符的，但是需要提供实现
 */
public interface OrderService {

    String name = "";

    String type = "";

    String createOrderNo();

    String getOrderInfo(String orderNo);

}
