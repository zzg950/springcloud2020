package com.zzg.spring.xml.service;

import java.util.UUID;

public class UserServiceImpl {

    private OrderService orderService;

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public String getOrderInfo() {
        return "user" + this.orderService.getOrderInfo(UUID.randomUUID().toString());
    }
}
