package com.zzg.spring.xml.service;


import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * 定义一个Service，使用注解标记
 */
@Service
public class OrderAnnotationServiceImpl implements OrderService {

    public void initMethod() {
        System.out.printf("initMethod : className = %s; name = %s; type = %s;%n", this.getClass().getSimpleName(), name, type);
    }

    @Override
    public String createOrderNo() {
        return this.getClass().getSimpleName() + " - " + UUID.randomUUID().toString();
    }

    @Override
    public String getOrderInfo(String orderNo) {
        return String.format(this.getClass().getSimpleName() + "; orderNo = %s; time = %d;", orderNo, System.currentTimeMillis());
    }
}
