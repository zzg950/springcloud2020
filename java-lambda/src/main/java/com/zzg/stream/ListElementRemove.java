package com.zzg.stream;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @Author 张志刚
 * @Date 2024/8/11
 * @Description TODO
 * 删除 List 中的元素
 * 在Java中，如果你想要在遍历一个List的同时删除其中不符合条件的数据，
 * 需要特别注意，因为直接在遍历过程中修改集合（如使用for-each循环）会导致ConcurrentModificationException异常。
 * 有几种方法可以实现你的需求，而避免这个异常。
 * 方法1: 使用Iterator的remove方法
 * 方法2: 使用Java 8的Stream API
 * 方法3: 创建一个新的List，循环原有List，将符合条件的写入新的List
 */
public class ListElementRemove {

    public static void main(String[] args) {
        ListElementRemove.removeElementByIterator();
        ListElementRemove.removeElementByStream();
        ListElementRemove.removeElementByBase();
    }

    public static List<Integer> getDemoList() {
        List<Integer> demoList = new ArrayList<>();
        IntStream.range(1, 100).forEach(i -> {
            demoList.add(i);
        });
        System.out.println(demoList);
        return demoList;
    }

    /**
     * 使用Iterator的hasNext()和next()方法来遍历集合，并使用remove()方法来删除元素。
     * 这是处理这类问题的推荐方式。
     */
    public static List<Integer> removeElementByIterator() {
        List<Integer> demoList = ListElementRemove.getDemoList();
        Iterator<Integer> iterator = demoList.iterator();
        while (iterator.hasNext()) {
            int num = iterator.next();
            if (num % 2 != 0) iterator.remove();
        }
        System.out.println(demoList);
        return null;
    }

    /**
     * 可以利用Stream API来过滤掉不符合条件的元素，并收集到一个新的List中。
     * 这种方法不会修改原始List，但你可以将结果赋值给原List或另一个变量。
     * 如果同时想要保留原有的List，可以使用此方法
     */
    public static List<Integer> removeElementByStream() {
        List<Integer> demoList = ListElementRemove.getDemoList();
        demoList = demoList.stream().filter(item -> item % 2 != 0).filter(item -> item % 3 == 0).collect(Collectors.toList());
        System.out.println(demoList);
        return demoList;
    }

    /**
     * JAVA最基本的方法处理，创建一个新的集合，循环原有集合，将符合条件的放入新的集合中
     * 此方法最通俗易懂，并且不破坏原有的集合，但是不够装逼
     */
    public static List<Integer> removeElementByBase() {
        List<Integer> demoList = ListElementRemove.getDemoList();
        List<Integer> newList = new ArrayList<>();
        for (Integer a : demoList) {
            if (a % 2 == 0) newList.add(a);
        }
        System.out.println(newList);
        return newList;
    }
}
